<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../layout/head.jsp" />

</head>
<body>
<jsp:include page="../layout/navbar.jsp" />

<div class="col-12 col-md-12 col-sm-12 col-lg-8 col-xl-8 rounded bg-light p-3 border mx-auto">
			<h1 class="h3 mb-3 font-weight-normal">
				<spring:message code="error.titulo" />
			</h1>
			<c:choose> <c:when test="${empty mensajePersonalizado}">
				<spring:message code="error.mensaje.generico" />
			</c:when>
			<c:otherwise>
			
				<spring:message code="${mensajePersonalizado}" />
			</c:otherwise>
			</c:choose>
</div>
</body>
</html>