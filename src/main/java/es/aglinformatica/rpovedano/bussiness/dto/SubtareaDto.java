package es.aglinformatica.rpovedano.bussiness.dto;

/**
 * Data Transfer Object de la entidad Subtarea
 * @author Rebeca Povedano
 *
 */
public class SubtareaDto {

	private Long id;
	private String nombre;
	private Long idUsuario;
	private boolean estado;
	
	 public SubtareaDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
}
