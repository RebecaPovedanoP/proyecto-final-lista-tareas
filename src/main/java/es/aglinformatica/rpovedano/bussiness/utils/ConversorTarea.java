package es.aglinformatica.rpovedano.bussiness.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.aglinformatica.rpovedano.bussiness.dao.tarea.TareaDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Etiqueta;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Tarea;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.bussiness.domain.UsuarioTarea;
import es.aglinformatica.rpovedano.bussiness.dto.TareaDto;
import es.aglinformatica.rpovedano.bussiness.service.usuario.UsuarioServiceInterface;
import es.aglinformatica.rpovedano.web.model.tarea.TareaFormModel;

/**
 * Componente que hace las conversiones entre las entidades, los dtos y los modelos para la entidad de Tarea
 * @author Rebeca Povedano
 *
 */
@Component
public class ConversorTarea {
	
	@Autowired
	private ConversorEtiqueta conversorEtiqueta;
	@Autowired
	private ConversorFecha conversorFecha;
	@Autowired
	private ConversorSubtarea conversorSubtarea;
	
	@Autowired
	private TareaDAOInterface tareaDAO;
	@Autowired
	private UsuarioServiceInterface usuarioService;
	
	
	public TareaDto entidadADto(Tarea tarea, Usuario usuario) {
		TareaDto dto=new TareaDto();
		dto.setId(tarea.getId());
		dto.setNombre(tarea.getNombre());
		dto.setEtiqueta(conversorEtiqueta.entidadADto(tarea.getEtiqueta()));
		if(tarea.getFechaInicio()!=null) {
			dto.setFechaInicio(conversorFecha.convertirFechaAString(tarea.getFechaInicio()));
		}
		if(tarea.getFechaFin()!=null) {
			dto.setFechaFin(conversorFecha.convertirFechaAString(tarea.getFechaFin()));
		}
		Boolean propietario=tareaDAO.checkTareaUsuarioPropietario(usuario.getId(), tarea.getId());
		dto.setPropietario(propietario !=null ? propietario.booleanValue() : false);
		return dto;
	}
	
	public List<TareaDto> listarTareaDto (List<Tarea> listaTarea, Usuario usuario) {
		List<TareaDto> listaDto=new ArrayList<TareaDto>();
		for(Tarea tarea : listaTarea) {
			listaDto.add(entidadADto(tarea, usuario));
		}
		return listaDto;
	}
	
	public Tarea modeloAEntidad(TareaFormModel modelo, Tablon tablon, Etiqueta etiqueta) {
		Tarea tarea=new Tarea();
		try {
		tarea.setId(modelo.getId());
		tarea.setNombre(modelo.getNombre());
		if(tarea.getFechaInicio()!=null) {
			tarea.setFechaInicio(conversorFecha.convertirStringaDate(modelo.getFechaInicio()));
		}
		if(tarea.getFechaFin()!=null) {
			tarea.setFechaFin(conversorFecha.convertirStringaDate(modelo.getFechaFin()));
		}
		tarea.setTablon(tablon);
		tarea.setEtiqueta(etiqueta);
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error al convertir la fecha");
		}
		return tarea;
	}
	
	public TareaFormModel entidadAModel(Tarea tarea) { 
		TareaFormModel modelo=new TareaFormModel();
		modelo.setId(tarea.getId());
		modelo.setNombre(tarea.getNombre());
		if(tarea.getFechaInicio()!=null) {
			modelo.setFechaInicio(conversorFecha.convertirFechaAString(tarea.getFechaInicio()));
		}
		if(tarea.getFechaFin()!=null) {
			modelo.setFechaFin(conversorFecha.convertirFechaAString(tarea.getFechaFin()));
		}
		modelo.setIdEtiqueta(tarea.getEtiqueta().getId());
		modelo.setListaUsuarios(usuarioService.listarUsuariosIdParaTarea(tarea.getId()));
		modelo.setListaSubTareas(conversorSubtarea.listaSubtareaDto(new ArrayList<>(tarea.getSubTareas())));
		return modelo;
	}
	
	public void modificarTarea(Tarea tarea, TareaFormModel modelo) {
		try {
		tarea.setId(modelo.getId());
		tarea.setNombre(modelo.getNombre());
		if(StringUtils.isNotBlank(modelo.getFechaInicio())) {
			tarea.setFechaInicio(conversorFecha.convertirStringaDate(modelo.getFechaInicio()));
		}
		if(StringUtils.isNotBlank(modelo.getFechaFin())) {
			tarea.setFechaFin(conversorFecha.convertirStringaDate(modelo.getFechaFin()));
		}
		
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error al convertir la fecha");
		}
	}
	
	public UsuarioTarea getUsuarioTarea(Usuario usuario, Tarea tarea) {
		UsuarioTarea usuarioTarea=new UsuarioTarea();
		usuarioTarea.setUsuario(usuario);
		usuarioTarea.setTarea(tarea);
		return usuarioTarea;
	}
	
	public List<UsuarioTarea> listarTareaUsuario (List<Usuario> listaUsuario, Usuario propietario, Tarea tarea) {
		List<UsuarioTarea> listaEntidad=new ArrayList<UsuarioTarea>();
		UsuarioTarea ut=new UsuarioTarea();
		ut.setTarea(tarea);
		ut.setUsuario(propietario);
		ut.setPropietario(true);
		listaEntidad.add(ut);
		for (Usuario usuario : listaUsuario) {
			listaEntidad.add(getUsuarioTarea(usuario,tarea));
		}
		return listaEntidad;
	}
}
