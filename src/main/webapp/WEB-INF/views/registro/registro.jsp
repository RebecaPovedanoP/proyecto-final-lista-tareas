<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<jsp:include page="../../layout/head.jsp" />
</head>
<body>
	<jsp:include page="../../layout/navbar.jsp" />
	<main role="main" class="container">
		<form:form action="registro" method="post"
			modelAttribute="usuarioModel"
			class="col-12 col-md-12 col-sm-12 col-lg-8 col-xl-8 rounded bg-light p-3 border mx-auto">
			<h1 class="h3 mb-3 font-weight-normal">
				<spring:message code="registro.titulo" />
			</h1>
			<div class="form-group">
				<form:label path="username">
					<spring:message code="registro.username" />
				</form:label>
				<form:input class="form-control" path="username" />
				<form:errors path="username" class="alert mt-2 alert-warning d-block p-2"/>
			</div>
			<div class="form-group">
				<form:label path="correo">
					<spring:message code="registro.correo" />
				</form:label>
				<form:input class="form-control" path="correo" />
				<form:errors path="correo" class="alert mt-2 alert-warning d-block p-2"/>
			</div>
			<div class="form-group">
				<form:label path="password">
					<spring:message code="registro.password" />
				</form:label>
				<form:password class="form-control" path="password" />
				<form:errors path="password" class="alert mt-2 alert-warning d-block p-2"/>
			</div>
			<div class="form-group">
				<form:label path="password">
					<spring:message code="registro.passConfirmacion" />
				</form:label>
				<form:password class="form-control" path="passwordConfirmacion" />
				<form:errors path="passwordConfirmacion" class="alert mt-2 alert-warning d-block p-2"/>
			</div>
			<button type="submit" class="mt-3 btn btn-lg btn-primary btn-block">
				<spring:message code="registro.boton.registro" />
			</button>
			<a href="<c:url value="/login" />"
				class="mt-3 btn btn-lg bg-info text-white btn-block"><spring:message
					code="login.titulo" /></a>
			<a href="<c:url value="/" />"
				class="mt-3 btn btn-lg btn-secondary btn-block"><spring:message
					code="registro.boton.atras" /></a>
		</form:form>
	</main>
</body>
</html>

