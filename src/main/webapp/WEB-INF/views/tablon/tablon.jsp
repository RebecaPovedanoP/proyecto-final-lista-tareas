<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
 <html>
 <head>
 
 <jsp:include page="../../layout/head.jsp" />
 <script>
 function postBorrar(id){
	 if(confirm("�Desea borrar este tablon?") ==true) {
		 var url=window.location.pathname+"/borrar/"+id;
		 $.post(url); 
	 }    
  }
 </script>
 </head>
 <body>
<jsp:include page="../../layout/navbar.jsp" />
<main role="main" class="container">
<h1 class="h3 mb-3 font-weight-normal"><spring:message code="tablon.titulo"/></h1>
<div class="clearfix"><div class="float-right">
 <c:url var="crear"  value="/tablon/crear" />
<a href="${crear}" class="mb-3 btn btn-lg btn-primary btn-block" >
  <spring:message code="tablon.boton.crear"/>
</a></div></div>
<div class="row mx-auto">
<c:forEach var="tablon" varStatus="status" items="${listaTablon}">
<div class="col-md-5 col-12 rounded bg-light p-3 border row ${status.count%2==0 ? 'offset-md-2' : ''} mb-5"><div class="col-9"><a href="<c:url value="/tablon/${tablon.id}"/>" class="text-dark h5"><c:out value="${tablon.nombre}"/></a></div> 
	<c:if test="${tablon.propietario}">
	<div class="col-3">
	<a href="<c:url value="/tablon/editar/${tablon.id}"/>" class="rounded-circle bg-dark text-light p-2 d-inline-block" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="tablon.editar"/>" ><i class="fas fa-edit"></i></a>
		<a href="javascript:postBorrar(${tablon.id})" class="borrarTablon rounded-circle bg-dark text-light p-2 d-inline-block" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="tablon.borrar"/>"><i class="fas fa-trash-alt"></i></a>
		</div>
	</c:if></div>
</c:forEach>
</div>
</main>

 </body>
 </html>
 
 