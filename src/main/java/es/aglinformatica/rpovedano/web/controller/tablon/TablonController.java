package es.aglinformatica.rpovedano.web.controller.tablon;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;
import es.aglinformatica.rpovedano.bussiness.dto.TablonDto;
import es.aglinformatica.rpovedano.bussiness.dto.UsuarioDto;
import es.aglinformatica.rpovedano.bussiness.service.tablon.TablonServiceInterface;
import es.aglinformatica.rpovedano.bussiness.service.usuario.UsuarioServiceInterface;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.tablon.TablonFormModel;
import es.aglinformatica.rpovedano.web.validador.TablonValidador;

/**
 * Controlador que realiza el mapeado de la página de los tablones y gestiona las diferentes peticiones get y post que en
 * ella se realizan 
 * @author Rebeca Povedano
 *
 */
@Controller
@RequestMapping("/tablon")
public class TablonController {

	@Autowired
	private TablonServiceInterface tablonService;
	@Autowired
	private UsuarioServiceInterface usuarioService;
	@Autowired
	private TablonValidador tablonValidador;
	
	private final String TABLON_JSP="/tablon/tablon";
	private final String TABLON_FORM_JSP="/tablon/tablonForm";

	private final String TABLON_REDIRECT="redirect:/tablon";
	private final String TAREA_REDIRECT="redirect:/tarea";
	
	/**
	 * GetMapping de la lista de tablones según el usuario
	 * @param model
	 * @param authentication
	 * @return
	 */
	@GetMapping("")
	public String getTablon(Model model, Authentication authentication) {	
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
			List<TablonDto> lista=tablonService.getListTablonDtoByUsuarioDetails(usuarioDetails);
			model.addAttribute("listaTablon", lista);
		}
		return TABLON_JSP;
	}
	
	/**
	 * GetMapping de un tablon en concreto, redirige al get de /tarea. Se usa la implementación UserDetails para almacenar
	 * el identificador del tablón y poder usarlo en otras partes de la aplicación, ya que es una entidad que mantiene 
	 * su sesión
	 * @param model
	 * @param idTablon
	 * @param authentication
	 * @return
	 */
	@GetMapping("/{idTablon}")
	public String getTablonById(Model model, @PathVariable(name="idTablon") Long idTablon, Authentication authentication) {		
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
			if(tablonService.hasPermission(usuarioDetails, idTablon)) {
				usuarioDetails.setIdTablon(idTablon);    
			} else {
				model.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
				return "error";
			}
		}
		return TAREA_REDIRECT;
	}
	
	/**
	 * Mapeado 
	 * @param model
	 * @param authentication
	 * @return
	 */
	@GetMapping("/crear")
	public String getCrearTablon(Model model, Authentication authentication) {			
			initTablon(model, authentication, new TablonFormModel());
		    return TABLON_FORM_JSP;		
		
	}
	
	/**
	 * Llamada a la acción de crear un tablón, que recoge el resultado del formulario, comprueba que cumple la validación
	 * y si lo hace, llama a la creación del tablón
	 * @param model
	 * @param tablonFormModel
	 * @param result
	 * @param authentication
	 * @return
	 */
	@PostMapping("/crear")
	public String postCreaTablon(Model model, @Valid @ModelAttribute("tablonFormModel") TablonFormModel tablonFormModel, BindingResult result, Authentication authentication) {		
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
			tablonValidador.validate(tablonFormModel, result);
			if(!result.hasErrors()) {
				tablonService.crearTablonByUsuario(tablonFormModel, usuarioDetails);
				return TABLON_REDIRECT;
			} else {
				initTablon(model, authentication, tablonFormModel);
				model.addAttribute("error", result.getFieldErrors());
			return TABLON_FORM_JSP;
			}
		} else {
			return "login";
		}
		
	}
	
	/**
	 * Método que mapea una petición post con un ResponseBody. Esto devuelve un json que es gestionado con una petición ajax
	 * haciendo uso de jQuery. Con esto se obtiene un json con los datos del objeto EtiquetaDto.
	 * @param model
	 * @param etiquetaDto
	 * @param result
	 * @return
	 */
	@PostMapping("/listaEtiqueta")
	public @ResponseBody EtiquetaDto postListaEitqueta(Model model, @Valid @ModelAttribute("etiquetaDto") EtiquetaDto etiquetaDto, BindingResult result) {
		if(result.hasErrors()) {
			return null;
		} else {
			return etiquetaDto;
		}
		
	}
	
	/**
	 * Método para cargar el model que muestra los datos del jsp. Como varias llamadas se repiten en ambos métodos
	 * se ha resumido todos ellos aquí. 
	 * @param model
	 * @param authentication
	 * @param tablonFormModel
	 * @param validado
	 * @param result
	 */
	private void initTablon (Model model, Authentication authentication, TablonFormModel tablonFormModel) {
		UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
	    List<UsuarioDto> listaUsuario=usuarioService.listarUsuarioDtoParaTablon(usuarioDetails);

	    model.addAttribute("etiquetaDto", new EtiquetaDto());
	    model.addAttribute("listaUsuario", listaUsuario);
	    model.addAttribute("tablonFormModel", tablonFormModel);
	}
	
	
	/**
	 * Método que gestiona la llamada post para borrar el tablón. Esta llamada se gestiona desde una petición ajax gestionada
	 * con jQuery que pide previamente la confirmación al usuario de que desea eliminar el tablon. Comprueba también que
	 * el usuario tenga permisos para realizar esta acción.
	 * @param idTablon
	 * @param authentication
	 * @param modelo
	 * @return
	 */
	@PostMapping("/borrar/{idTablon}")
	public String postBorrarTablon(@PathVariable("idTablon") Long idTablon, Authentication authentication, Model modelo) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
			if(tablonService.hasPermission(usuarioDetails, idTablon)) {
				tablonService.borrarTablon(idTablon);
				return TABLON_REDIRECT;
			} else {
				modelo.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
				return "error";
			}
		} 
		return TABLON_JSP;
	}
	
	/**
	 * Méodo que obtiene el formulario para editar el tablón, según la idea del mismo.
	 * @param idTablon
	 * @param model
	 * @param authentication
	 * @return
	 */
	@GetMapping("/editar/{idTablon}")
	public String getEditaTablon(@PathVariable("idTablon") Long idTablon, Model model, Authentication authentication) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
			if(tablonService.hasPermission(usuarioDetails, idTablon)) {
				TablonFormModel tablonFormModel=tablonService.getTablonModelById(idTablon, usuarioDetails);
				tablonFormModel.setEditar(true);
				initTablon(model, authentication, tablonFormModel);
			    return TABLON_FORM_JSP;	
			} else {
				model.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
				return "error";
			}
		} 
		return TABLON_JSP;
	}
	
	
	/**
	 * Método que gestiona la acción post del formulario. Realiza la validación de los campos y comprueba también que el
	 * usuario tenga permiso para realizar esa acción.
	 * @param model
	 * @param tablonFormModel
	 * @param result
	 * @param authentication
	 * @return
	 */
	@PostMapping("/editar/{idTablon}")
	public String postEditaTablon(Model model, @Valid @ModelAttribute("tablonFormModel") TablonFormModel tablonFormModel, BindingResult result, Authentication authentication) {		
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
			tablonValidador.validate(tablonFormModel, result);
			if(!result.hasErrors()) {
				tablonService.modificarTablon(tablonFormModel, usuarioDetails);
				return TABLON_REDIRECT;
			} else {
				initTablon(model, authentication, tablonFormModel);
				model.addAttribute("error", result.getFieldErrors());
			return TABLON_FORM_JSP;
			}
		} else {
			return TABLON_FORM_JSP;
		}
		
	}
}
