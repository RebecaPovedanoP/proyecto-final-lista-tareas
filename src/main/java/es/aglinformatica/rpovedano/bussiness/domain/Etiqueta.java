package es.aglinformatica.rpovedano.bussiness.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="etiqueta")
public class Etiqueta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "etiqueta_seq")
	@SequenceGenerator(name="etiqueta_seq", initialValue=1, allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@NotNull
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="color")
	private String color;
	
	public Etiqueta() {
		color="GREY";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
	@Override
	public boolean equals(Object o) {
	    if (o == null || this.getClass() != o.getClass()) {
	        return false;
	    }
	    Etiqueta e=(Etiqueta) o;
	    return e.id.equals(this.id);
	}
}
