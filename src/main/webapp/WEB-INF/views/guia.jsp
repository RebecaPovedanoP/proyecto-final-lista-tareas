<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../layout/head.jsp" />

</head>
<body>
<jsp:include page="../layout/navbar.jsp" />
<main role="main" class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-8 col-xl-8 m-2 rounded bg-light p-3 border mx-auto">
			<h1 class="h3 mt-3 font-weight-normal">
				Gu�a de la aplicaci�n
			</h1>
			<h1 class="h4 mt-3 font-weight-normal">
			1. Acceso
			</h1>
			<div>Usted dentra acceso a la p�gina de incio de la aplicaci�n, as� como est� gu�a que est� viendo en pantalla. 
			Para poder disfrutar completamente de todas las funcionalidades, debe proceder al registro. </div>
			<h1 class="h4 mt-3 font-weight-normal">
			2. Registro
			</h1>
			<div>Si ya dispone de una cuenta de usuario, puede pasar al punto 3 de esta gu�a. En caso contrario, continue su lectura. 
			
			<br/>Para proceder al registro, deber� pinchar en el enlace de la barra superior, el cual se llama Registro. Le llevar� a la p�gina 
			para realizar el registro:
			<img src="<c:url value="/resources/imagen/registro.PNG" />" class="img-fluid my-2"/>
			
			<br/>Podr� ver que necesita insertar un usuario, un correo y una contrase�a, que debe ser verificada posteriormente. Si introduce
			un usuario o correo repetido, la aplicaci�n de registro le notifica que no puede registrarse, as� como si no cumple los requisitos
			establecidos para la correcta construcci�n de la contrase�a.
			
			<img src="<c:url value="/resources/imagen/registrovalidacion.PNG" />" class="img-fluid my-2"/>
			Una vez introducidos los datos correctamente, proceda a darle al bot�n de Registrarse. Le llevar� a la p�gina del login, que veremos en el punto siguiente. </div>
			
			<h1 class="h4 mt-3 font-weight-normal">
			3. Login
			</h1>
			<div>
			Una vez se ha registrado en la aplicaci�n, puede proceder a ingresar en la misma. El registro le redirigir� autom�ticamente 
			a la p�gina de ingreso, pero siempre puede acceder desde el enlace de Conectate que se encuentra en la barra superior.
			
			<img src="<c:url value="/resources/imagen/login.PNG" />" class="img-fluid my-2"/>
			
			Para ello, escriba su usuario y contrase�a en la formulario de conexi�n. Si falta alg�n campo por rellenar, la aplicaci�n le avisar� de ello.
			
			<img src="<c:url value="/resources/imagen/credenciales.PNG" />" class="img-fluid my-2"/>
			
			La aplicaci�n est� securizada, de forma que no acceder� a la misma si las credenciales no son correctas. No se preocupe, la aplicaci�n le avisar� de ello.
			<img src="<c:url value="/resources/imagen/loginvalidacion.PNG" />" class="img-fluid my-2"/>
			Una vez haya accedido a la aplicaci�n, se encontrar� de nuevo en el �ndice. Podr� usar los botones de la barra superior para navegar por la misma.
			</div>
			
			<h1 class="h4 mt-3 font-weight-normal">
			3. Tablones <i class="fas fa-table"></i>
			</h1>
			<div>
			Pulsando en el icono: <i class="fas fa-table"></i> podr� acceder a sus tablones personales. No se preocupes si se encuentra la p�gina vac�a. Es normal si se acaba de registra. Puede proceder a crear un 
			tabl�n pulsando en el bot�n de Crear tabl�n
			<img src="<c:url value="/resources/imagen/tablonvacio.PNG" />" class="img-fluid my-2"/>
			Le llevar� a la p�gina de creaci�n del tabl�n, que es la siguiente:
			<img src="<c:url value="/resources/imagen/tabloncreacion.PNG" />" class="img-fluid my-2"/>
			Como se observa en la imagen, podr� ponerle un nombre al tabl�n, a�adirle usuarios que quiera que colaboren para usted y a�adirle las etiquetas que vaya a usar.
			Es obligatorio que tenga un nombre y al menos una etiqueta. A la hora de escoger los usuarios, puede arrastrar el rat�n o usar su tecla de control para seleccionar varios.
			Recuerde que si no cumple con los requisitos, al darle al bot�n de Guardar tabl�n, la aplicaci�n le notificar� de ello:
			
			<img src="<c:url value="/resources/imagen/tablonvalidacion.PNG" />" class="img-fluid my-2"/>
			Para a�adir una etiqueta, inserte el nombre de la misma, escoja el color en el colorpicker que sale justo a la izquierda del campo y pulse en A�adir nueva etiqueta. Podr� ver el resultado
			que se ha generado a continuaci�n:
						<img src="<c:url value="/resources/imagen/tablonetiqueta.PNG" />" class="img-fluid my-2"/>
			Una vez haya rellenado todos los campos, puede proceder a guardar. Se le generar� el tabl�n que ha creado. Puede crear m�s. Quiz� se encuentre con el hecho de que 
			alg�n usuario le ha elegido para acceder a su tabl�n. 
						<img src="<c:url value="/resources/imagen/tablones.PNG" />" class="img-fluid my-2"/>
			Si usted ha creado los tablones, ver� unos botones. Con dichos botones puede proceder a la edici�n y al borrado del tabl�n. La edici�n funciona de la misma forma que la
			 creaci�n, simplemente modificar� los datos ya generados.
						<img src="<c:url value="/resources/imagen/tabloneditar.PNG" />" class="img-fluid my-2"/>						
						<img src="<c:url value="/resources/imagen/tablonborrar.PNG" />" class="img-fluid my-2"/>
			 <br/> Con el borrado, se eliminar�n todas las entidades vinculadas a los tablones, las posibles tareas y sus correspondientes subtareas, as� como las etiquetas asignadas. Es una 
			 acci�n irreversible, por lo que tenga cuidado al realizarla. Antes de hacerlo, le saldr� un aviso de confirmaci�n:
			 						
						<img src="<c:url value="/resources/imagen/borrarconfirmacion.PNG" />" class="img-fluid my-2"/>
						
			</div>
			<h1 class="h4 mt-3 font-weight-normal">
			3. Tareas 
			</h1>
			<div>
			Si pulsa en el nombre de cualquiera de los tablones, acceder� a la lista de tareas propiamente dicho. No se preocupes si le aparece vacio, es normal para un tabl�n reci�n creado.
									<img src="<c:url value="/resources/img/tareasvacio.PNG" />" class="img-fluid my-2"/>
			Para crear su primera tarea pulse en el bot�n de  Crear nueva tarea, que le llevar� al formulario de creaci�n de la tarea.
			Al igual que en el caso del tabl�n, si falta alg�n dato, la aplicaci�n le notificar� de ello.
						<img src="<c:url value="/resources/img/tareavalidacion.PNG" />" class="img-fluid my-2"/>
			Para a�adir una nueva subtarea, rellene el campo del nombre y pulse sobre A�adir nueva subtarea. Ver� como se ha creado el registro de la nueva subtarea.
						
						<img src="<c:url value="/resources/img/subtarea.PNG" />" class="img-fluid my-2"/>
						Una vez est� satisfecho con el resultado y el formulario est� debidamente cumplimentado, puede proceder a darle al bot�n de Guardar tarea, que almacenar� la misma en la base de datos.<br/>
						Ya cuenta con nuevas tareas, que se ver�n de la siguiente forma: 
						<img src="<c:url value="/resources/img/listadotareas.PNG" />" class="img-fluid my-2"/>
			Ver� que las tareas tambi�n cuentan con unos botones de edici�n y borrado. Funcionan de la misma forma que los botones para el tabl�n.
			</div>
			<h1 class="h4 mt-3 font-weight-normal">
			4. Tareas pendientes <i class="far fa-list-alt"></i>
			</h1>
			<div>
			En el apartado de tareas pendientes podr� encontrar todas aquellas tareas cuya fecha de finalizaci�n no est� fijada o bien aun no haya llegado. Funciona de la misma
			forma que el listado normal, simplemente no est�n organizadas por tablones.
			</div>
			<h1 class="h4 mt-3 font-weight-normal">
			5. Colaboradores <i class="fas fa-users"></i>
			</h1>
			<div>
			En esta p�gina se mostrar�n todos los usuarios con los que comparta tablones en com�n.
			</div>
			
			<h1 class="h4 mt-3 font-weight-normal">
			6. Desconexi�n <i class="fas fa-users"></i>
			</h1>
			<div>
			Para desconectarse de la p�gina, simplemente proceda a pulsar el enlace que encontrar� en la barra superior a la derecha, que dice Desconectate.
			</div>
		</div>
	</main>
</body>
</html>