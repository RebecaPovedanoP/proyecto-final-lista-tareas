package es.aglinformatica.rpovedano.web.validador;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.aglinformatica.rpovedano.bussiness.utils.ConversorFecha;
import es.aglinformatica.rpovedano.web.model.tarea.TareaFormModel;

/**
 * Validador para la clase TareaFormModel. Realiza las validaciones de las fechas, que estén en un formato correcto,
 * que la fecha de inicio no sea mayor que la fecha de finalización, que se seleccione una etiqueta de forma obligatoria
 * y que el nombre  de la etiqueta no pueda estar vacio.
 * @author Rebeca Povedano
 *
 */
@Component
public class TareaValidador implements Validator {

	@Autowired
	private ConversorFecha conversorFecha;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return TareaFormModel.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		TareaFormModel modelo=(TareaFormModel) target;
		
		validarNombre(modelo,errors);
		validarFecha(modelo, errors);
		validarEtiqueta(modelo, errors);
	}

	private void validarEtiqueta(TareaFormModel modelo, Errors errors) {
		if(modelo.getIdEtiqueta()==null) {
			errors.rejectValue("idEtiqueta", "input.text.error.vacio");
		}
	}

	private void validarFecha(TareaFormModel modelo, Errors errors) {
	
		if(StringUtils.isNotBlank(modelo.getFechaInicio())) {
			if(formatoFecha(modelo.getFechaInicio())) {
				errors.rejectValue("fechaInicio", "tarea.input.fecha");
			} 
		}
		if(StringUtils.isNotBlank(modelo.getFechaFin())) {
			if(formatoFecha(modelo.getFechaFin())) {
				errors.rejectValue("fechaFin", "tarea.input.fecha");
			}
		}
		
		if(StringUtils.isNotBlank(modelo.getFechaInicio()) && StringUtils.isNotBlank(modelo.getFechaFin())) {
			if(!formatoFecha(modelo.getFechaInicio()) && !formatoFecha(modelo.getFechaFin())) {
				comprobarFechas(modelo.getFechaInicio(),modelo.getFechaFin(),errors);
			}
		}
	}

	private void comprobarFechas(String fechaInicio, String fechaFin, Errors errors) {
		
		try {
			Date fechaIn=conversorFecha.convertirStringaDate(fechaInicio);
			Date fechaFi=conversorFecha.convertirStringaDate(fechaFin);
			if(fechaIn.after(fechaFi) ) {
				errors.rejectValue("fechaInicio", "tarea.input.fecha.posterior");
				errors.rejectValue("fechaFin", "tarea.input.fecha.anterior");
			}
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error al convertir las fechas");
		}
	}

	private boolean formatoFecha(String fecha) {
		boolean formatoIncorrecto=true;
		try {
			conversorFecha.convertirStringaDate(fecha);
			formatoIncorrecto=false;
		} catch (Exception e) {
			formatoIncorrecto=true;
		}
		return formatoIncorrecto;
	}

	private void validarNombre(TareaFormModel modelo, Errors errors) {
		if(StringUtils.isBlank(modelo.getNombre())) {
			errors.rejectValue("nombre", "input.text.error.vacio");
		}
	}

}
