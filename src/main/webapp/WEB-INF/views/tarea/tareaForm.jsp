<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<jsp:include page="../../layout/head.jsp" />
<script type="text/javascript"
	src="<c:url value="/resources/js/tarea/tareaForm.js" />"></script>
<script>
	$(document).ready(function() {
		$('.selectpicker').selectpicker();

		$('[data-provide="datepicker"]').datepicker({
			language : 'es',
			format : 'dd-mm-yyyy',
			clearBtn : true
		});
	});
</script>
<script type="text/javascript"
	src="<c:url value="/resources/js/tablon/tablonForm.js" />"></script>
</head>
<body>
	<jsp:include page="../../layout/navbar.jsp" />
	<main role="main" class="container">
		<div
			class="col-12 col-md-12 col-sm-12 col-lg-8 col-xl-8 rounded bg-light p-3 pb-5 border mx-auto">
			<form:form method="post" modelAttribute="tareaFormModel">
			<h1 class="h3 mb-3 font-weight-normal">
				<spring:message code="${tareaFormModel.editar ? 'tarea.boton.editar': 'tarea.boton.crear'}" />
			</h1>
				<div class="form-group row">
					<div class="col-6">
						<form:hidden path="id" />
						<form:label path="nombre">
							<spring:message code="tarea.form.nombre" />
						</form:label>
						<form:input class="form-control" path="nombre" />
						<form:errors path="nombre"
							class="alert mt-2 alert-warning d-block p-2" />
					</div>
					<div class="col-6">
						<form:label path="idEtiqueta">
							<spring:message code="tarea.form.etiqueta" />
						</form:label>
						<form:select path="idEtiqueta" class="selectpicker"
							data-style="bg-white text-dark form-control border btn-block">
							<form:option value="">Seleccione una opci�n</form:option>
							<c:forEach items="${listaEtiquetas}" var="etiqueta">
								<form:option value="${etiqueta.id}"
									data-content="<span class='btn' style='background: ${etiqueta.color}'></span>  ${etiqueta.nombre}"></form:option>
							</c:forEach>
						</form:select>
						<form:errors path="idEtiqueta"
							class="alert mt-2 alert-warning d-block p-2" />
					</div>
					<div class="col-6 mt-3">
						<form:label path="fechaInicio">
							<spring:message code="tarea.form.fecha.inicio" />
						</form:label>
						<form:input class="form-control date" readonly="true"
							data-provide="datepicker" path="fechaInicio" />
						<form:errors path="fechaInicio"
							class="alert mt-2 alert-warning d-block p-2" />
					</div>
					<div class="col-6 mt-3">
						<form:label path="fechaFin">
							<spring:message code="tarea.form.fecha.fin" />
						</form:label>
						<form:input class="form-control date" data-provide="datepicker"
							readonly="true" path="fechaFin" />
						<form:errors path="fechaFin"
							class="alert mt-2 alert-warning d-block p-2" />
					</div>
				</div>

				<c:if test="${!empty listaUsuarios}">
					<div class="form-group">
						<form:label path="listaUsuarios">
							<spring:message code="tarea.form.usuario" />
						</form:label>
						<form:select path="listaUsuarios" class="form-control"
							items="${listaUsuarios}" itemLabel="username" itemValue="id"
							multiple="true" />

					</div>
				</c:if>
				<div id="listaSubtarea" class="row">
					<c:if test="${!empty tareaFormModel.listaSubTareas}">
					<div class="col-5"><label id="subtarea-nombre">
										<spring:message code="tarea.form.subTarea" />
									</label></div>
					<div class="col-5"><label id="subtarea-user">
										<spring:message code="tarea.form.subtarea.usuario" />
									</label></div>
					<div class="col-2"><label id="subtarea-check">
										<spring:message code="tarea.form.subTarea.hecho" />
									</label></div>
						<c:forEach items="${tareaFormModel.listaSubTareas}" var="subtarea"
							varStatus="n">
								<div class="col-5 mb-2">
									
									<form:input class="form-control subtarea-nombre"
										path="listaSubTareas[${n.index}].nombre" readonly="${subtarea.estado}"/>
								</div>
								<div class="col-5 mb-2">
									<form:select id="listadoUsuario" path="listaSubTareas[${n.index}].idUsuario"
										class="form-control" items="${listaUsuarios}"
										itemLabel="username" itemValue="id" disabled="true"/>
								</div>
								<div class="col-2 mb-2 form-check text-center">
									<form:checkbox path="listaSubTareas[${n.index}].estado" disabled="${etiqueta.hecho}"/>
								</div>
						</c:forEach>
					</c:if>
				</div>

				<div class="submitForm">
					<input type="submit" class="btn-primary btn btn-lg btn-block" value="<spring:message code="tarea.form.guardar"/>" />
				</div>
			</form:form>
			<c:url var="json" value="/tarea/listaSubtarea" />

			<form:form id="subTarea" class="pb-3" action="${json}" method="post"
				modelAttribute="subtareaDto">
				<div class="form-inline">
				
					<div class="form-group col-5 pl-0">
						<input type="submit" class="btn btn-info btn-block"
							value="<spring:message code="tarea.form.subTarea.boton"/>" />


					</div>
					<div
						class="form-group col-7 px-0">

						<spring:message code="tarea.form.subTarea"
							var="placeholderEtiqueta" />
						<form:input path="nombre"
							class="form-control ${empty listaUsuario ? 'col-12' : ''}"
							placeholder="${placeholderSubtarea}" />
						<form:errors path="nombre" />
					</div>
					

				</div>
			</form:form>
		</div>
	</main>
</body>
</html>

