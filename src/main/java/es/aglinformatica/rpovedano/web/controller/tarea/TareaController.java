package es.aglinformatica.rpovedano.web.controller.tarea;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;
import es.aglinformatica.rpovedano.bussiness.dto.SubtareaDto;
import es.aglinformatica.rpovedano.bussiness.dto.TareaDto;
import es.aglinformatica.rpovedano.bussiness.dto.UsuarioDto;
import es.aglinformatica.rpovedano.bussiness.service.etiqueta.EtiquetaServiceInterface;
import es.aglinformatica.rpovedano.bussiness.service.tablon.TablonServiceInterface;
import es.aglinformatica.rpovedano.bussiness.service.tarea.TareaServiceInterface;
import es.aglinformatica.rpovedano.bussiness.service.usuario.UsuarioServiceInterface;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.tablon.TablonFormModel;
import es.aglinformatica.rpovedano.web.model.tarea.TareaFormModel;
import es.aglinformatica.rpovedano.web.validador.TareaValidador;

/**
 * Controlador que realiza el mapeado de la página de los tablones y gestiona
 * las diferentes peticiones get y post que en ella se realizan
 * 
 * @author Rebeca Povedano
 *
 */
@Controller
@RequestMapping("/tarea")
public class TareaController {

	@Autowired
	private TareaServiceInterface tareaService;
	@Autowired
	private EtiquetaServiceInterface etiquetaService;
	@Autowired
	private UsuarioServiceInterface usuarioService;
	@Autowired
	private TablonServiceInterface tablonService;
	@Autowired
	private TareaValidador validador;

	private final String TAREA_FORM_JSP = "/tarea/tareaForm";

	private final String TAREA_JSP = "/tarea/lista-tarea";

	private final String TAREA_REDIRECT = "redirect:/tarea";

	/**
	 * Mapping de la petición get para obtener la lista de tareas para el usuario y
	 * asignadas a un tablón
	 * 
	 * @param model
	 * @param authentication
	 * @return
	 */
	@GetMapping("")
	public String getListaTareas(Model model, Authentication authentication) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails = (UsuarioDetails) authentication.getPrincipal();
			Long idTablon = usuarioDetails.getIdTablon();
			if (tablonService.hasPermission(usuarioDetails, idTablon)) {
				List<TareaDto> lista = tareaService.getListTareaDtoByTablonId(idTablon, usuarioDetails);
				List<EtiquetaDto> listaEtiquetas = etiquetaService.listarEtiquetasByTablon(idTablon);
				model.addAttribute("listaEtiquetas", listaEtiquetas);
				model.addAttribute("listaTarea", lista);
				model.addAttribute("tareaFormModel", new TablonFormModel());
			} else {

				model.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
				return "error";
			}
		}
		return TAREA_JSP;
	}

	/**
	 * Mapeado de la llamada al formulario de creación de una tarea.
	 * 
	 * @param model
	 * @param authentication
	 * @return
	 */
	@GetMapping("/crear")
	public String getCrearTarea(Model model, Authentication authentication) {
		UsuarioDetails usuarioDetails = (UsuarioDetails) authentication.getPrincipal();
		Long idTablon = usuarioDetails.getIdTablon();
		if (tablonService.hasPermission(usuarioDetails, idTablon)) {
			model.addAttribute("tareaFormModel", new TareaFormModel());
			initTareaModel(model, idTablon, usuarioDetails);
		} else {
			model.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
			return "error";
		}
		return TAREA_FORM_JSP;
	}

	/**
	 * Método post la acción del formulario para crear una nueva tarea. Incluye una
	 * validación personalizada para comprobar que los campos se han rellenado
	 * correctamente.
	 * 
	 * @param model
	 * @param tareaFormModel
	 * @param result
	 * @param authentication
	 * @return
	 */
	@PostMapping("/crear")
	public String postCrearTarea(Model model, @ModelAttribute("tareaFormModel") TareaFormModel tareaFormModel,
			BindingResult result, Authentication authentication) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails = (UsuarioDetails) authentication.getPrincipal();
			Long idTablon = usuarioDetails.getIdTablon();
			if (tablonService.hasPermission(usuarioDetails, idTablon)) {
				validador.validate(tareaFormModel, result);
				if (!result.hasErrors()) {
					tareaService.createTarea(idTablon, usuarioDetails, tareaFormModel);
					return TAREA_REDIRECT;
				} else {
					model.addAttribute("error", result.getFieldErrors());
					initTareaModel(model, idTablon, usuarioDetails);
					return TAREA_FORM_JSP;
				}

			} else {
				model.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
				return "error";
			}
		}
		return TAREA_FORM_JSP;

	}

	/**
	 * Llamada post a la acción de listaSubtarea, que permite añadir al formulario
	 * de creación de las tareas los datos de una subtarea.
	 * 
	 * @param model
	 * @param subtareaDto
	 * @param result
	 * @return
	 */
	@PostMapping("/listaSubtarea")
	public @ResponseBody SubtareaDto postListaEtiqueta(Model model,
			@Valid @ModelAttribute("subtareaDto") SubtareaDto subtareaDto, BindingResult result) {
		if (result.hasErrors()) {
			return null;
		} else {
			return subtareaDto;
		}

	}

	@GetMapping("/editar/{idTarea}")
	public String getEditarTarea(Model model, @PathVariable("idTarea") Long idTarea, Authentication authentication) {
		UsuarioDetails usuarioDetails = (UsuarioDetails) authentication.getPrincipal();
		Long idTablon = usuarioDetails.getIdTablon();
		if (tareaService.hasPermission(usuarioDetails, idTarea)) {
			TareaFormModel tareaFormModel = tareaService.getTareaModelById(idTarea, usuarioDetails);
			tareaFormModel.setEditar(true);
			model.addAttribute("tareaFormModel", tareaFormModel);
			initTareaModel(model, idTablon, usuarioDetails);
		} else {
			model.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
			return "error";
		}
		return TAREA_FORM_JSP;
	}

	@PostMapping("/editar/{idTarea}")
	public String postEditarTarea(Model model, @PathVariable("idTarea") Long idTarea,
			@ModelAttribute("tareaFormModel") TareaFormModel tareaFormModel, BindingResult result,
			Authentication authentication) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails = (UsuarioDetails) authentication.getPrincipal();
			Long idTablon = usuarioDetails.getIdTablon();
			validador.validate(tareaFormModel, result);
			if (tareaService.hasPermission(usuarioDetails, idTarea)) {
				if (!result.hasErrors()) {
					tareaService.modificarTarea(usuarioDetails, tareaFormModel);
					return TAREA_REDIRECT;
				} else {
					model.addAttribute("error", result.getFieldErrors());
					initTareaModel(model, idTablon, usuarioDetails);
					return TAREA_FORM_JSP;
				}
			} else {
				model.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
				return "error";
			}

		} else {
			return TAREA_FORM_JSP;
		}
	}

	/**
	 * Método que gestiona la llamada post para borrar la tarea. Esta llamada se
	 * gestiona desde una petición ajax gestionada con jQuery que pide previamente
	 * la confirmación al usuario de que desea eliminarla tarea. Comprueba también
	 * que el usuario tenga permisos para realizar esta acción.
	 * 
	 * @param idTarea
	 * @param authentication
	 * @param modelo
	 * @return
	 */
	@PostMapping("/borrar/{idTarea}")
	public String postBorrarTablon(@PathVariable("idTarea") Long idTarea, Authentication authentication, Model modelo) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails = (UsuarioDetails) authentication.getPrincipal();
			if (tareaService.hasPermission(usuarioDetails, idTarea)) {
				tareaService.borrarTarea(idTarea);
				return "redirect:/tarea";
			} else {
				modelo.addAttribute("mensajePersonalizado", "error.mensaje.permisos");
				return "error";
			}
		}
		return TAREA_JSP;
	}

	/**
	 * Método que permite cargar el model que muestra los datos del jsp. Como varias
	 * llamadas se repiten en ambos métodos se ha resumido todos ellos aquí.
	 * 
	 * @param model
	 * @param idTablon
	 * @param usuarioDetails
	 */
	private void initTareaModel(Model model, Long idTablon, UsuarioDetails usuarioDetails) {
		List<EtiquetaDto> listaEtiquetas = etiquetaService.listarEtiquetasByTablon(idTablon);
		List<UsuarioDto> listaUsuarios = usuarioService.listarUsuarioDtoParaTarea(usuarioDetails, idTablon);
		model.addAttribute("listaEtiquetas", listaEtiquetas);
		model.addAttribute("listaUsuarios", listaUsuarios);
		model.addAttribute("subtareaDto", new SubtareaDto());
	}
}
