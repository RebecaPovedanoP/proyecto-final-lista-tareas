<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../layout/head.jsp" />

</head>
<body>
<jsp:include page="../layout/navbar.jsp" />
<main role="main" class="container">
<h1 class="h3 mb-3 font-weight-normal">
				<spring:message code="colaboradores.titulo" />
			</h1>
			<div class="row mx-auto">
<c:forEach items="${lista}" var="usuario" varStatus="status" >
<div class="col-md-5 col-12 rounded bg-light p-3 border row ${status.count%2==0 ? 'offset-md-2' : ''} mb-5"><div class="col-9"><c:out value="${usuario.username}"/></div></div>
</c:forEach>
<c:if test="${vacio}"><spring:message code="colaboradores.vacio"/></c:if>
</div>
</main>
</body>
</html>