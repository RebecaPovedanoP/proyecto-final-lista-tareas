package es.aglinformatica.rpovedano.bussiness.dao.tarea;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.aglinformatica.rpovedano.bussiness.domain.UsuarioTarea;

@Repository
public interface UsuarioTareaDAOInterface extends JpaRepository<UsuarioTarea, Long> {

}
