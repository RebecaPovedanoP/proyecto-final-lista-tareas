package es.aglinformatica.rpovedano.bussiness.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="etiqueta_tablon")
public class EtiquetaTablon implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "etiquetatablon_seq")
	@SequenceGenerator(name="etiquetatablon_seq", initialValue=1, allocationSize=1)
	private Long id;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="id_etiqueta")
	private Etiqueta etiqueta;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="id_tablon")
	private Tablon tablon;
	

	public EtiquetaTablon() {
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Etiqueta getEtiqueta() {
		return etiqueta;
	}


	public void setEtiqueta(Etiqueta etiqueta) {
		this.etiqueta = etiqueta;
	}


	public Tablon getTablon() {
		return tablon;
	}


	public void setTablon(Tablon tablon) {
		this.tablon = tablon;
	}
	
	@Override
	public boolean equals(Object o) {
	    if (o == null || this.getClass() != o.getClass()) {
	        return false;
	    }
	    EtiquetaTablon e=(EtiquetaTablon) o;
	    return e.id.equals(this.id);
	}
}
