package es.aglinformatica.rpovedano.web.controller.usuario;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import es.aglinformatica.rpovedano.bussiness.service.usuario.UsuarioServiceInterface;
import es.aglinformatica.rpovedano.web.model.usuario.UsuarioLoginFormModel;
import es.aglinformatica.rpovedano.web.model.usuario.UsuarioRegistroFormModel;
import es.aglinformatica.rpovedano.web.validador.RegistroValidador;

/**
 * Controlador que realiza el mapeado del registro y conexión del usuario en la aplicación
 * @author Rebeca Povedano
 *
 */
@Controller
@RequestMapping("/")
public class UsuarioController {

	private final String REDIRECT_LOGIN_EXITOSO="redirect:/";
	private final String LOGIN_JSP="/login/login";
	private final String LOGIN_URL="login";
	private final String REGISTRO_JSP="/registro/registro";
	private final String REGISTRO_URL="registro";
	private final String REDIRECT_REGISTRO_EXITOSO="redirect:/login";
	
	@Autowired
	private UsuarioServiceInterface usuarioService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private RegistroValidador registroValidador;
	
	/**
	 * GetMapping del login del usuario. Verifica que el usuario no esté ya logado
	 * @param model
	 * @return
	 */
	@GetMapping(LOGIN_URL)
	public String getLogin(Model model) {	
		model.addAttribute("usuarioModel", new UsuarioLoginFormModel());
		Authentication authentication =SecurityContextHolder.getContext().getAuthentication();
		
		if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return LOGIN_JSP;
        } else {
		return REDIRECT_LOGIN_EXITOSO;
        }
	}
	
	/**
	 * Gestiona el post del login. Tiene una validación para comprobar que se han rellenado los campos correctamente
	 * @param model
	 * @param usuarioDto
	 * @param result
	 * @return
	 */
	@PostMapping(LOGIN_URL)
	public String postLogin(Model model, @Valid @ModelAttribute("usuarioModel") UsuarioLoginFormModel usuarioDto, BindingResult result) {
		
		if (result.hasErrors()) {
			model.addAttribute("error", result.getFieldErrors());
	        return LOGIN_JSP;
	    }
		try {
			UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(usuarioDto.getUsername(), usuarioDto.getPassword());

		    // Authenticate the user
		    Authentication authentication = authenticationManager.authenticate(authRequest);
		    SecurityContext securityContext = SecurityContextHolder.getContext();
		    securityContext.setAuthentication(authentication);
		  
			return REDIRECT_LOGIN_EXITOSO;
		} catch (Exception e) {
			model.addAttribute("badCredentials", true);
            return LOGIN_JSP;
		}
		
	}
	
	/**
	 * GetMapping del login del usuario. Verifica que el usuario no esté ya logado
	 * @param model
	 * @return
	 */
	@GetMapping(REGISTRO_URL)
	public String getRegistro (Model model) {
		model.addAttribute("usuarioModel", new UsuarioRegistroFormModel());
		Authentication authentication =SecurityContextHolder.getContext().getAuthentication();
		
		if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return REGISTRO_JSP;
        } else {
		return REGISTRO_URL;
        } 
	}
	
	/**
	 * Gestiona el post del registro. Tiene una validación para comprobar que se han rellenado los campos correctamente
	 * @param model
	 * @param usuarioDto
	 * @param result
	 * @return
	 */
	@PostMapping(REGISTRO_URL) 
	public String postRegistro (Model model, @ModelAttribute("usuarioModel") UsuarioRegistroFormModel usuarioDto, BindingResult result){
		registroValidador.validate(usuarioDto, result);
		if (result.hasErrors()) {
			model.addAttribute("error", result.getFieldErrors());
            return REGISTRO_JSP;
        } else {
        	usuarioService.crearUser(usuarioDto);
    		return REDIRECT_REGISTRO_EXITOSO;
        }
        
		
	}
}
