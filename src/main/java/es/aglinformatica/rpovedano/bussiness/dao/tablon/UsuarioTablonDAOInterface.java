package es.aglinformatica.rpovedano.bussiness.dao.tablon;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.aglinformatica.rpovedano.bussiness.domain.UsuarioTablon;

@Repository
public interface UsuarioTablonDAOInterface extends JpaRepository<UsuarioTablon, Long> {

}
