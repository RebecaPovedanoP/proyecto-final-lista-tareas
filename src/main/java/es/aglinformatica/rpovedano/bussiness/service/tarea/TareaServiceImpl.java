package es.aglinformatica.rpovedano.bussiness.service.tarea;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import es.aglinformatica.rpovedano.bussiness.dao.etiqueta.EtiquetaDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tablon.TablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tarea.SubtareaDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tarea.TareaDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tarea.UsuarioTareaDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Etiqueta;
import es.aglinformatica.rpovedano.bussiness.domain.Subtarea;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Tarea;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.bussiness.domain.UsuarioTarea;
import es.aglinformatica.rpovedano.bussiness.dto.TareaDto;
import es.aglinformatica.rpovedano.bussiness.utils.ConversorSubtarea;
import es.aglinformatica.rpovedano.bussiness.utils.ConversorTarea;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.tarea.TareaFormModel;

@Service
public class TareaServiceImpl implements TareaServiceInterface {

	@Autowired
	private TablonDAOInterface tablonDAO;
	@Autowired
	private TareaDAOInterface tareaDAO;
	@Autowired
	private UsuarioDAOInterface usuarioDAO;
	@Autowired
	private EtiquetaDAOInterface etiquetaDAO;
	@Autowired
	private UsuarioTareaDAOInterface usuarioTareaDAO;
	@Autowired
	private SubtareaDAOInterface subtareaDAO;
	
	@Autowired
	private ConversorTarea conversorTarea;
	@Autowired
	private ConversorSubtarea conversorSubtarea;

	@Override
	public List<TareaDto> getListTareaDtoByTablonId(Long idTablon, UsuarioDetails usuarioDetails) {
		Optional<Tablon> optionalTablon = tablonDAO.findTablonById(idTablon);
		List<TareaDto> lista = new ArrayList<TareaDto>();
		Optional<Usuario> optionalUsuario = usuarioDAO.findById(usuarioDetails.getId());
		if (optionalUsuario.isPresent()) {
			if (optionalTablon.isPresent()) {
				Tablon tablon = optionalTablon.get();
				List<Tarea> listaTarea = tareaDAO.findAllTareasByTablon(tablon);
				lista = conversorTarea.listarTareaDto(listaTarea, optionalUsuario.get());
			}
		}
		return lista;
	}

	@Override
	@Transactional
	public void createTarea(Long idTablon, UsuarioDetails usuarioDetails, TareaFormModel tareaFormModel) {

		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(usuarioDetails.getId());
		if (optionalUser.isPresent()) {
			Optional<Tablon> optionalTablon = tablonDAO.findById(idTablon);
			Optional<Etiqueta> optionalEtiqueta = etiquetaDAO.findById(tareaFormModel.getIdEtiqueta());
			if (optionalTablon.isPresent() && optionalEtiqueta.isPresent()) {
				Tarea tarea = conversorTarea.modeloAEntidad(tareaFormModel, optionalTablon.get(),
						optionalEtiqueta.get());
				List<Subtarea> listaSubtarea=conversorSubtarea.listaSubtarea(tareaFormModel.getListaSubTareas(), tarea, optionalUser.get());
				if(!CollectionUtils.isEmpty(listaSubtarea)) {
				tarea.setSubTareas(new HashSet<Subtarea>(listaSubtarea));
				}
				List<Usuario> listaUsuario = usuarioDAO.findUsuariosByIds(tareaFormModel.getListaUsuarios());
				List<UsuarioTarea> listaUsuarioTarea = conversorTarea.listarTareaUsuario(listaUsuario,optionalUser.get(), tarea);
				if(!CollectionUtils.isEmpty(listaUsuarioTarea)) {
					usuarioTareaDAO.saveAll(listaUsuarioTarea);
				}
				
				if(!CollectionUtils.isEmpty(listaSubtarea)) {
					subtareaDAO.saveAll(listaSubtarea);
				}
			}
		}
	}


	@Override
	public TareaFormModel getTareaModelById(Long idTarea, UsuarioDetails propietario) {
		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(propietario.getId());
		if (optionalUser.isPresent()) {
			Optional<Tarea> optional = tareaDAO.findById(idTarea);
			if (optional.isPresent()) {
				Tarea tarea = optional.get();
				TareaFormModel modelo = conversorTarea.entidadAModel(tarea);
				return modelo;

			}
		}	return null;
	}
	
	@Override
	public List<TareaDto> getListTareaDtoPendiente(UsuarioDetails usuarioDetails) {
		List<Tarea> listaTarea = tareaDAO.findTareasPendientes(usuarioDetails.getId(),new Date());
		Optional<Usuario> optionalUsuario = usuarioDAO.findById(usuarioDetails.getId());
		if (optionalUsuario.isPresent()) {
			List<TareaDto> listaDto = conversorTarea.listarTareaDto(listaTarea, optionalUsuario.get());
			return listaDto;
		}
		return null;
	}


	@Override
	public boolean hasPermission(UsuarioDetails usuarioDetails, Long id) {
		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(usuarioDetails.getId());
		if (optionalUser.isPresent()) {
			Usuario usuario = optionalUser.get();
			Optional<Tarea> optionalTarea = tareaDAO.findTareaByUsuarioAndTareaId(usuario, id);
			return optionalTarea.isPresent();
		} else {
			return false;
		}
	}
	
	@Override
	@Transactional
	public void modificarTarea(UsuarioDetails usuarioDetails, TareaFormModel tareaFormModel) {
		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(usuarioDetails.getId());
		if (optionalUser.isPresent()) {
			Optional<Tarea> optional = tareaDAO.findById(tareaFormModel.getId());
			if (optional.isPresent()) {
				Tarea tarea = optional.get();		
				subtareaDAO.deleteAll(tarea.getSubTareas());				
				List<Usuario> listaUsuario=usuarioDAO.findUsuariosByIds(tareaFormModel.getListaUsuarios());		
				List<UsuarioTarea> listaUsuarioTarea = conversorTarea.listarTareaUsuario(listaUsuario,optionalUser.get(), tarea);
				usuarioTareaDAO.deleteAll(tarea.getUsuarioTarea());				
				conversorTarea.modificarTarea(tarea, tareaFormModel);
				List<Subtarea> listaSubtarea=conversorSubtarea.listaSubtarea(tareaFormModel.getListaSubTareas(), tarea, optionalUser.get());
				tarea.setSubTareas(new HashSet<Subtarea>(listaSubtarea));
				tareaDAO.save(tarea);
				usuarioTareaDAO.flush();
				if(!CollectionUtils.isEmpty(listaUsuarioTarea)) {
					usuarioTareaDAO.saveAll(listaUsuarioTarea);
				}
				if(!CollectionUtils.isEmpty(listaSubtarea)) {
					subtareaDAO.saveAll(listaSubtarea);
				}
			}
		}
		
	}

	@Override
	public void borrarTarea(Long id) {
		Optional<Tarea> optional=tareaDAO.findById(id);
		if(optional.isPresent()) {
			tareaDAO.delete(optional.get());
		}
	}


}
