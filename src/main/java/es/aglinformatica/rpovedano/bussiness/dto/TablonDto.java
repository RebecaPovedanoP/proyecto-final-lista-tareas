package es.aglinformatica.rpovedano.bussiness.dto;

/**
 * Data Transfer Object de la entidad Tablon
 * @author Rebeca Povedano
 *
 */
public class TablonDto {

	private Long id;
	private String nombre;
	private boolean propietario;
	
	public TablonDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isPropietario() {
		return propietario;
	}

	public void setPropietario(boolean propietario) {
		this.propietario = propietario;
	}
	
}
