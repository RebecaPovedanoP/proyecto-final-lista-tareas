package es.aglinformatica.rpovedano.bussiness.service.usuario;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;

/**
 * Implementación de la clase de UserDetailsService, usada para gestionar el acceso por parte de los usuarios a la aplicación
 * @author Rebeca Povedano
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioDAOInterface usuarioDAO;
	
	/**
	 * Override del método loadUserByUsername, que busca al usuario según el username con el que se esté tratando de conectar.
	 * Si no lo encuentra, lanza una excepción de tipo UsernameNotFoundException. Permitó la conexión y gestionar las
	 * credenciales de la parte de la conexión de la aplicación.
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Optional<Usuario> optionalUsuario=usuarioDAO.findUsuarioByUsername(username);
		
		if(!optionalUsuario.isPresent()) {
			throw new UsernameNotFoundException(username);
		} 
		return new UsuarioDetails(optionalUsuario.get());
	}
}
