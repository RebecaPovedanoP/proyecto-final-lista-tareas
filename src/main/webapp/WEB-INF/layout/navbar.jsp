<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<div class="collapse navbar-collapse navbar-brand" id="navbar-collapse">
<ul class="navbar-nav mr-auto">
<li class="nav-item active"><a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="navbar.label.indice"/>" href="<c:url value="/" />"><i class="fas fa-home"></i> <span class="navbar-collapse-span"><spring:message code="navbar.label.indice"/></span></a></li>
<sec:authorize access="isAuthenticated()">
<li class="nav-item"><a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="navbar.label.tablon"/>" href="<c:url value="/tablon" />"><i class="fas fa-table"></i> <span class="navbar-collapse-span"><spring:message code="navbar.label.tablon"/></span></a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="navbar.label.pendientes"/>" href="<c:url value="/tarea-pendiente" />"><i class="far fa-list-alt"></i> <span class="navbar-collapse-span"><spring:message code="navbar.label.pendientes"/></span></a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="navbar.label.colaborador"/>" href="<c:url value="/colaborador" />"><i class="fas fa-users"></i> <span class="navbar-collapse-span"><spring:message code="navbar.label.colaborador"/></span></a></li>
</sec:authorize>
<li class="nav-item"><a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="navbar.label.guia"/>" href="<c:url value="/guia" />"><i class="fas fa-question-circle"></i> <span class="navbar-collapse-span"><spring:message code="navbar.label.guia"/></span></a></li>

</ul>
</div>
<div class="navbar-brand"><sec:authorize access="!isAuthenticated()"><a href="<c:url value="/registro" />" class="text-white"><spring:message code="navbar.label.registro"/></a> | <a href="<c:url value="login" />" class="text-white"><spring:message code="navbar.label.login"/></a></sec:authorize>
<sec:authorize access="isAuthenticated()">
   
  
    <sec:authentication property="principal.username"/> |

<a class="text-white" href="<c:url value="/logout" />"><spring:message code="navbar.label.logout"/></a>


</sec:authorize></div>
</nav>

<script>
$(document).ready(function() {
	  $('li.active').removeClass('active');
	  $('a[href="' + location.pathname + '"]').closest('li').addClass('active'); 
	});
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>