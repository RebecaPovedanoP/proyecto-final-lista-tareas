package es.aglinformatica.rpovedano.web.validador;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.web.model.usuario.UsuarioRegistroFormModel;

/**
 * Validador para la clase UsuarioRegistroFormModel. Comprueba que los campos no
 * estén vacios. También se encarga de comprobar que el username ni el correo
 * estén repetidos. Comprueba que las contraseñas sean identicas y que coincidan
 * con el patrón para tener al menos una mayúsucula y al menos un digito
 * 
 * @author Rebeca Povedano
 *
 */
@Component
public class RegistroValidador implements Validator {

	// Patron
	private String patron = "^(?=(.*[A-Z]))(?=(.*[a-z]))(?=(.*[0-9]))((.*[@#$%^!&+=.\\-_*]))*.{8,}$";
	@Autowired
	UsuarioDAOInterface usuarioDAO;

	@Override
	public boolean supports(Class<?> clazz) {
		return UsuarioRegistroFormModel.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		UsuarioRegistroFormModel modelo = (UsuarioRegistroFormModel) target;
		validarUsername(modelo, errors);
		validarCorreo(modelo, errors);
		validarPass(modelo, errors);
	}

	private void validarPass(UsuarioRegistroFormModel modelo, Errors errors) {
		if (StringUtils.isAllBlank(modelo.getPassword())) {
			errors.rejectValue("password", "input.text.error.vacio");
		}
		if (StringUtils.isAllBlank(modelo.getPasswordConfirmacion())) {
			errors.rejectValue("passwordConfirmacion", "input.text.error.vacio");
		}
		if (StringUtils.isNotBlank(modelo.getPassword()) && StringUtils.isNotBlank(modelo.getPasswordConfirmacion())) {
			if (!modelo.getPassword().equals(modelo.getPasswordConfirmacion())) {
				errors.rejectValue("password", "registro.input.pass.error.coinciden");
				errors.rejectValue("passwordConfirmacion", "registro.input.pass.error.coinciden");
			} else {
				if (!modelo.getPassword().matches(patron)) {
					errors.rejectValue("password", "registro.input.pass.error.patron");
					errors.rejectValue("passwordConfirmacion", "registro.input.pass.error.patron");
				}
			}
		}

	}

	private void validarCorreo(UsuarioRegistroFormModel modelo, Errors errors) {
		if (StringUtils.isNotBlank(modelo.getCorreo())) {
			Optional<Usuario> optional = usuarioDAO.findUsuarioByCorreo(modelo.getCorreo());
			if (optional.isPresent()) {
				errors.rejectValue("correo", "registro.input.correo.error.existe");
			}
		} else {
			errors.rejectValue("correo", "input.text.error.vacio");
		}
	}

	private void validarUsername(UsuarioRegistroFormModel modelo, Errors errors) {
		if (StringUtils.isNotBlank(modelo.getUsername())) {
			Optional<Usuario> optional = usuarioDAO.findUsuarioByUsername(modelo.getUsername());
			if (optional.isPresent()) {
				errors.rejectValue("username", "registro.input.nombre.error.existe");
			}
		} else {
			errors.rejectValue("username", "input.text.error.vacio");
		}

	}

}
