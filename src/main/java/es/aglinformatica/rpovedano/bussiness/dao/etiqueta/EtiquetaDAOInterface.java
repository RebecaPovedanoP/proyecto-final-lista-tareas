package es.aglinformatica.rpovedano.bussiness.dao.etiqueta;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.aglinformatica.rpovedano.bussiness.domain.Etiqueta;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;

@Repository
public interface EtiquetaDAOInterface extends JpaRepository<Etiqueta, Long> {

	/**
	 * Devuelve la lista de las etiquetas asociadas a un tablón
	 * @param tablon
	 * @return
	 */
	@Query("select e from Etiqueta e left join EtiquetaTablon et on e=et.etiqueta where et.tablon=:tablon")
	public List<Etiqueta> listarEtiquetaByTablon(@Param("tablon") Tablon tablon);
	
}
