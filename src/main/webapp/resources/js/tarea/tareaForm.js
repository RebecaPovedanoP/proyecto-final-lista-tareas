$( document ).ready(function() {
    $('#subTarea').submit(function( event ) {
	  event.preventDefault();
	  var action=$(this).attr("action");
	  var formData=$(this).serialize();
	  $.post(action, formData, function(data) {
		  if(data.nombre!=null && data.nombre!=="") {			
		  $('#validacionMensaje').remove();
		  $('#etiquetaTabla').removeClass("d-none");		  
		  $('h1.h5').removeClass("d-none");
		  var n=$('.subtarea-nombre').length;
		  console.log($('label#subtarea-nombre').length);
		  if($('label#subtarea-nombre').length==0) {
		   $('#listaSubtarea').append('<div class="col-5"><label id="#subtarea-nombre">Nombre de la subtarea</label></div>');
		   }
	
		   
		  var nombre='<div class="col-12 m-"><input name="listaSubTareas['+n+'].nombre" class="form-control subtarea-nombre" type="text" value="'+data.nombre+'"></div>'
		  $('#listaSubtarea').append(nombre);
		 	
		  $('#subTarea input#nombre').val('')			
		  
	  } else {
		  if($('#validacionMensaje').length==0) {
		  	  $('#listaSubtarea').after("<div id='validacionMensaje' class='mb-3 alert mt-2 alert-warning d-block p-2'>El nombre de la subtarea es obligatorio</div>");
		  
		  } else {
		  $('#validacionMensaje').remove();
		  }
	  }
		  });
	});

});