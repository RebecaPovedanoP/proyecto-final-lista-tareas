package es.aglinformatica.rpovedano.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Clase que gestiona la configuración la seguridad de la aplicación y que hereda de WebSecurityConfigurerAdapter
 * @author Rebeca
 *
 */

@Configuration
/**
 * Permite que se escanen los diferentes componentes, que son aquellas clases marcadas con las etiquetas de component,
 * repository, controller, service, etc. Permite realizar un autowired en ellas, lo cual no hace necesario que sean 
 * instanciadas en ningún momento. 
 * @author Rebeca Povedano
 *
 */
@ComponentScan(basePackages = {"es.aglinformatica.rpovedano.*"})
/**
 * Se encarga de indicarle a la aplicación donde se encuentran los pojos
 * @author Rebeca
 *
 */
@EntityScan(basePackages = {"es.aglinformatica.rpovedano.bussiness.domain"} )
/**
 * Le indica a la aplicación en que paquete están las interfaces marcadas como repository
 * @author Rebeca
 *
 */
@EnableJpaRepositories(basePackages = {"es.aglinformatica.rpovedano.bussiness.dao"})
/**
 * Permite el uso de la parte de Spring security
 * @author Rebeca
 *
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsService detailsService;
	

	/**
	 * Método que hace override a configure. Permite seleccionar que clases puede ver el usuario sin conectarse a la aplicación,
	 * en este caso, puede ver el indice, el login y el registro. También asigna que vista se va a encargar de realizar el login y
	 * que acción post va a gestionar eso.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
			.antMatchers("/","/login", "/registro","/guia").permitAll()
			.antMatchers("/resources/**").permitAll()
			.antMatchers("/resources/imagen/**").permitAll()
			.anyRequest().authenticated()
			.and()
		.formLogin()
			.loginPage("/login")
			.permitAll()
			.loginProcessingUrl("/login")
			.loginProcessingUrl("/loginProcess") //permite usar un PostMapping del login personalizado
			.usernameParameter("username")
			.passwordParameter("password")
			.defaultSuccessUrl("/",true)
			.and()
		.logout().logoutSuccessUrl("/");
		http.csrf().disable();
	}
	
	/**
	 * Bean obligatorio en Spring Security, que crear un encoder. Lo que hace el encoder es encriptar las contraseñas. 
	 * @return
	 */
	@Bean
	public PasswordEncoder encoder() {
	    return new BCryptPasswordEncoder();
	}
	
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
	    auth.userDetailsService(detailsService).passwordEncoder(encoder());
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
	    return super.authenticationManagerBean();
	}
}
