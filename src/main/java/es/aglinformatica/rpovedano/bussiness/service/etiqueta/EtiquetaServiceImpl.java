package es.aglinformatica.rpovedano.bussiness.service.etiqueta;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.aglinformatica.rpovedano.bussiness.dao.etiqueta.EtiquetaDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tablon.TablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Etiqueta;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;
import es.aglinformatica.rpovedano.bussiness.utils.ConversorEtiqueta;

@Service
public class EtiquetaServiceImpl implements EtiquetaServiceInterface {

	@Autowired
	private EtiquetaDAOInterface etiquetaDAO;
	@Autowired
	private TablonDAOInterface tablonDAO;
	
	@Autowired
	private ConversorEtiqueta conversorEtiqueta;
	
	
	@Override
	public List<EtiquetaDto> listarEtiquetasByTablon(Long idTablon) {
		List<EtiquetaDto> lista=new ArrayList<EtiquetaDto>();
		Optional<Tablon> optionaTablon=tablonDAO.findById(idTablon);
		if(optionaTablon.isPresent()) {
			Tablon tablon=optionaTablon.get();
			List<Etiqueta> listaEtiqueta=etiquetaDAO.listarEtiquetaByTablon(tablon);
			lista=conversorEtiqueta.listaEtiquetaDto(listaEtiqueta);
		}
		
		return lista;
	}

}
