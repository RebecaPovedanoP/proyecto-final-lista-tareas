package es.aglinformatica.rpovedano.config;


import java.util.Locale;


import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


/**
 * Clase encargada de realizar la configuración para poder arrancar la aplicación en un servidor de aplicaciones como
 * tomcat. 
 * 
 * @author Rebeca Povedano
 *
 */
@Configuration
/**
 * Permite que se escanen los diferentes componentes, que son aquellas clases marcadas con las etiquetas de component,
 * repository, controller, service, etc. Permite realizar un autowired en ellas, lo cual no hace necesario que sean 
 * instanciadas en ningún momento. 
 * @author Rebeca Povedano
 *
 */
@ComponentScan(basePackages = {"es.aglinformatica.rpovedano.*"})
/**
 * Se encarga de indicarle a la aplicación donde se encuentran los pojos
 * @author Rebeca
 *
 */
@EntityScan(basePackages = {"es.aglinformatica.rpovedano.bussiness.domain"} )
/**
 * Le indica a la aplicación en que paquete están las interfaces marcadas como repository
 * @author Rebeca
 *
 */
@EnableJpaRepositories(basePackages = {"es.aglinformatica.rpovedano.bussiness.dao"})
@EnableAutoConfiguration
public class Inicializador extends SpringBootServletInitializer {

  /**
   * Permite añadir los recursos a la parte web de la aplicación, como hojas de estilos o ficheros javascript
   * @param registry
   */
	public void addResourceHandlers(ResourceHandlerRegistry registry) 
	{
	        registry.addResourceHandler("/resources/**")
	                .addResourceLocations("/resources/");
	}
	
	/**
	 * Método que se encarga de ejecutar la aplicación
	 */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Inicializador.class);
    }

    /**
     * Bean encargado de gestionar la dirección de la vistas. Si la vista del indice se encuentra en la direccion 
     * ./WEB-INF/views/index.jsp, tan solo bastará con poner index para poder mapearla.
     * @return
     */
    @Bean
    public ViewResolver getInternalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
    
    /**
     * Se encarga de asignar que el idioma de la aplicación es el español
     * @return
     */
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(new Locale("es"));
        return slr;
    }
    
    /**
     * Carga el fichero de messages.properties, con todos los mensajes que van a usarse en la capa de presentación de la
     * aplicación
     * @return
     */
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource
          = new ReloadableResourceBundleMessageSource();
        
        messageSource.setBasename("classpath:i18n/messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
    
    /**
     * Permite realizar validación dentro de la aplicación, como validar los campos de un formulario
     * @return
     */
    @Bean
    public LocalValidatorFactoryBean getValidator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }
    
    /**
     * Permite la carga de un script como base de datos en local.
     * 
     */
    
   /* @Bean
    public DataSource dataSource(){
        return
            (new EmbeddedDatabaseBuilder())
            .addScript("classpath:database/script.sql")
            .build();
    } */
}
