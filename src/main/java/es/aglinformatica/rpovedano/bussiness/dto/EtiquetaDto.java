package es.aglinformatica.rpovedano.bussiness.dto;

/**
 * Data Transfer Object de la entidad Etiqueta
 * @author Rebeca Povedano
 *
 */
public class EtiquetaDto {

	private Long id;
	private String nombre;
	private String color;
	
	public EtiquetaDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
}
