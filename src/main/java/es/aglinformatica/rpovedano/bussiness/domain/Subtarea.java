package es.aglinformatica.rpovedano.bussiness.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="subtarea")
public class Subtarea implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "subtarea_seq")
	@SequenceGenerator(name="subtarea_seq", initialValue=1, allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@NotNull
	@Column(name="nombre", length=100)
	private String nombre;
	
	@Column(name="estado")
	private boolean estado;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tarea")
	private Tarea tarea;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	public Subtarea() {
	}

	public Subtarea(String nombre, boolean estado, Tarea tarea, Usuario usuario) {
		this.nombre = nombre;
		this.estado = estado;
		this.tarea = tarea;
		this.usuario = usuario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Tarea getTarea() {
		return tarea;
	}

	public void setTarea(Tarea tarea) {
		this.tarea = tarea;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	
}
