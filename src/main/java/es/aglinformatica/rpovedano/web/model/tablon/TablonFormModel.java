package es.aglinformatica.rpovedano.web.model.tablon;

import java.util.List;
import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;

/**
 * Clase que modela el formulario para la creación y/o modificación del tablon
 * @author Rebeca Povedano
 *
 */
public class TablonFormModel {
	private Long id;

	private String nombre;

	private List<Long> lista;

	private List<EtiquetaDto> listaEtiqueta;
	
	private boolean editar;
	
	public TablonFormModel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Long> getLista() {
		return lista;
	}

	public void setLista(List<Long> lista) {
		this.lista = lista;
	}

	public List<EtiquetaDto> getListaEtiqueta() {
		return listaEtiqueta;
	}

	public void setListaEtiqueta(List<EtiquetaDto> listaEtiqueta) {
		this.listaEtiqueta = listaEtiqueta;
	}

	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}
}
