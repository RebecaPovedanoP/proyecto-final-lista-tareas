package es.aglinformatica.rpovedano.web.model.usuario;


/**
 * Clase que modela el formulario para el registro del usuario
 * @author Rebeca Povedano
 *
 */
public class UsuarioRegistroFormModel {

	private Long id;
	
	private String username;
	
	private String correo;
	
	private String password;
	
	private String passwordConfirmacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirmacion() {
		return passwordConfirmacion;
	}

	public void setPasswordConfirmacion(String passwordConfirmacion) {
		this.passwordConfirmacion = passwordConfirmacion;
	}
	
	
	
}
