package es.aglinformatica.rpovedano.bussiness.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.aglinformatica.rpovedano.bussiness.domain.Etiqueta;
import es.aglinformatica.rpovedano.bussiness.domain.EtiquetaTablon;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;

/**
 * Componente que hace las conversiones entre las entidades, los dtos y los modelos para la entidad de etiquetas
 * @author Rebeca Povedano
 *
 */
@Component
public class ConversorEtiqueta {

	
	public EtiquetaDto entidadADto(Etiqueta etiqueta) {
		EtiquetaDto dto=new EtiquetaDto();
		dto.setId(etiqueta.getId());
		dto.setNombre(etiqueta.getNombre());
		dto.setColor(etiqueta.getColor());
		return dto;
	}
	
	public List<EtiquetaDto> listaEtiquetaDto(List<Etiqueta> listaEtiqueta) {
		List<EtiquetaDto> listaDto=new ArrayList<EtiquetaDto>();
		for(Etiqueta etiqueta : listaEtiqueta) {
			listaDto.add(entidadADto(etiqueta));			
		}
		return listaDto;
	}
	public Etiqueta dtoAEntidad(EtiquetaDto dto) {
		Etiqueta etiqueta=new Etiqueta();
		etiqueta.setId(dto.getId());
		etiqueta.setNombre(dto.getNombre());
		etiqueta.setColor(dto.getColor());
		return etiqueta;
	}
	
	public EtiquetaTablon dtoAEtiquetaTablon (EtiquetaDto dto, Tablon tablon) {
		EtiquetaTablon etiquetaTablon=new EtiquetaTablon();
		etiquetaTablon.setEtiqueta(dtoAEntidad(dto));
		etiquetaTablon.setTablon(tablon);
		return etiquetaTablon;
	}
	
	public List<EtiquetaTablon> listaEtiquetaTablon (List<EtiquetaDto> listaDto, Tablon tablon) {
		List<EtiquetaTablon> lista=new ArrayList<EtiquetaTablon>();
		for(EtiquetaDto dto : listaDto) {
			lista.add(dtoAEtiquetaTablon(dto, tablon));
		}
		return lista;
	}
}
