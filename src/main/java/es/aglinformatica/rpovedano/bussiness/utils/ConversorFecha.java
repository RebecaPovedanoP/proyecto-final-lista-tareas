package es.aglinformatica.rpovedano.bussiness.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * Componente que convierte un string en una fecha y viceversa
 * @author Rebeca Povedano
 *
 */
@Component
public class ConversorFecha {

	public String convertirFechaAString(Date fecha) {
		SimpleDateFormat formato=new SimpleDateFormat("dd-MM-yyyy");
		formato.setLenient(false);
		String fechaString=formato.format(fecha);
		return fechaString;
	}
	
	public Date convertirStringaDate(String fecha) throws ParseException {
		SimpleDateFormat formato=new SimpleDateFormat("dd-MM-yyyy");
		formato.setLenient(false);
		Date fechaDate=formato.parse(fecha);
		return fechaDate;
	}
}
