package es.aglinformatica.rpovedano.bussiness.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Subtarea;
import es.aglinformatica.rpovedano.bussiness.domain.Tarea;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.bussiness.dto.SubtareaDto;

/**
 * Componente que hace las conversiones entre las entidades, los dtos y los
 * modelos para la entidad de Subtarea
 * 
 * @author Rebeca Povedano
 *
 */
@Component
public class ConversorSubtarea {

	@Autowired
	public UsuarioDAOInterface usuarioDAO;

	public Subtarea dtoAEntidad(SubtareaDto dto, Tarea tarea, Usuario propietario) {
		Subtarea subtarea = new Subtarea();
		subtarea.setId(dto.getId());
		subtarea.setNombre(dto.getNombre());
		subtarea.setEstado(dto.isEstado());
		subtarea.setTarea(tarea);
		if (dto.getIdUsuario() != null) {
			Optional<Usuario> optionalUsuario = usuarioDAO.findById(dto.getIdUsuario());
			if (optionalUsuario.isPresent()) {
				subtarea.setUsuario(optionalUsuario.get());
			} else {
				subtarea.setUsuario(propietario);
			}
		} else {

			subtarea.setUsuario(propietario);
		}
		return subtarea;
	}

	public List<Subtarea> listaSubtarea(List<SubtareaDto> listaDto, Tarea tarea, Usuario propietario) {
		List<Subtarea> lista = new ArrayList<Subtarea>();
		if(!CollectionUtils.isEmpty(listaDto)) {
		for (SubtareaDto dto : listaDto) {
			lista.add(dtoAEntidad(dto, tarea, propietario));
		}
		}
		return lista;
	}
	
	public SubtareaDto entidadADto(Subtarea subtarea) {
		SubtareaDto dto=new SubtareaDto();
		dto.setId(subtarea.getId());
		dto.setIdUsuario(subtarea.getUsuario().getId());
		dto.setEstado(subtarea.isEstado());
		dto.setNombre(subtarea.getNombre());
		return dto;
	}
	
	public List<SubtareaDto> listaSubtareaDto (List<Subtarea> listaEntidad) {
		List<SubtareaDto> listaDto=new ArrayList<SubtareaDto>();
		for(Subtarea subtarea : listaEntidad) {
			listaDto.add(entidadADto(subtarea));
		}
		return listaDto;
	}
}
