<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<jsp:include page="../../layout/head.jsp" />
<script type="text/javascript"
	src="<c:url value="/resources/js/tablon/tablonForm.js" />"></script>
</head>
<body>
	<jsp:include page="../../layout/navbar.jsp" />
	<main role="main" class="container">
		<div
			class="col-12 col-md-12 col-sm-12 col-lg-8 col-xl-8 rounded bg-light p-3 pb-5 border mx-auto">
			
			


			<form:form method="post"
				modelAttribute="tablonFormModel" id="tablonForm">
				<h1 class="h3 mb-3 font-weight-normal">
			
				<spring:message code="${tablonForm.editar ? 'tablon.editar' : 'tablon.boton.crear'}" />
			</h1>
				<form:hidden path="id"/>
				<div class="form-group">
					<form:label path="nombre">
						<spring:message code="tablon.form.nombre" />
					</form:label>
					<form:input class="form-control" path="nombre" />
					<form:errors path="nombre"
						class="alert mt-2 alert-warning d-block p-2" />
				</div>
				<div class="form-group">
					<form:label path="lista">
						<spring:message code="tablon.form.usuario" />
					</form:label>
					<form:select path="lista" class="form-control"
						items="${listaUsuario}" itemLabel="username" itemValue="id"
						multiple="true" />

				</div>
				<div class="form-group">
				<h1 class="h5 mb-3 font-weight-normal ${empty tablonFormModel.listaEtiqueta ? 'd-none' : ''}">
				<spring:message code="tablon.form.etiqueta.lista" />
			</h1>
					<table id="etiquetaTabla"
						class="table table-striped table-sm table-hover ${empty tablonFormModel.listaEtiqueta ? 'd-none' : ''}">
						<thead class="thead-light">
							<tr>
								<th><spring:message code="tablon.form.etiqueta.nombre"/></th>
								<th><spring:message code="tablon.form.etiqueta.color"/></th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${!empty tablonFormModel.listaEtiqueta}">
								<c:forEach items="${tablonFormModel.listaEtiqueta}"
									var="etiqueta" varStatus="n">
									<tr>
										<td><c:out value="${etiqueta.nombre}" /></td>
										<td>
										<div class="btn col-8 p-3" style="background:${etiqueta.color}"></div></td>										
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
					<c:if test="${!empty tablonFormModel.listaEtiqueta}">
						<c:forEach items="${tablonFormModel.listaEtiqueta}" var="etiqueta"
							varStatus="n">
							<input name="listaEtiqueta[${n.index}].nombre"
								class="listaEtiqueta nombre" type="hidden"
								value="${etiqueta.nombre}">
							<input name="listaEtiqueta[${n.index}].color"
								class="listaEtiqueta color" type="hidden"
								value="${etiqueta.color}">
								
						</c:forEach>
					</c:if>
				</div>
				<div class="submitForm">
					<input type="submit" class="btn btn-primary btn-block"
						value="<spring:message code="tablon.form.guardar"/>" />
				</div>
	<form:errors id="validacionMensaje" class="alert mt-2 alert-warning d-block p-2" path="listaEtiqueta"/>
			</form:form>

			<c:url var="json" value="/tablon/listaEtiqueta" />

			<form:form id="etiquetaTablon" action="${json}"
				method="post" modelAttribute="etiquetaDto">
				<div class="form-inline">
					<div class="form-group col-8 px-0">
						<div class="input-group col-12 pl-0">
							<div class="input-group-prepend col-2 mx-auto px-0">
								<form:input type="color" path="color"
									class="input-group-text form-control col-12" />
								<form:errors path="color" />
							</div>
						<spring:message code="tablon.form.etiqueta.nombre" var="placeholderEtiqueta" />
							<form:input path="nombre" class="form-control"
								placeholder="${placeholderEtiqueta}" />
							<form:errors path="nombre" />
						</div>
					</div>
					<div class="form-group col-4 pr-0">
						<input type="submit" class="btn btn-info btn-block"
							value="<spring:message code="tablon.form.etiqueta.agregar"/>"  />


					</div>

				</div>
			</form:form>

		</div>
	</main>
</body>
</html>

