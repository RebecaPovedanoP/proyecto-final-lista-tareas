package es.aglinformatica.rpovedano.bussiness.service.tablon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import es.aglinformatica.rpovedano.bussiness.dao.etiqueta.EtiquetaTablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tablon.TablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tablon.UsuarioTablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.EtiquetaTablon;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.bussiness.domain.UsuarioTablon;
import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;
import es.aglinformatica.rpovedano.bussiness.dto.TablonDto;
import es.aglinformatica.rpovedano.bussiness.service.etiqueta.EtiquetaServiceInterface;
import es.aglinformatica.rpovedano.bussiness.service.usuario.UsuarioServiceInterface;
import es.aglinformatica.rpovedano.bussiness.utils.ConversorEtiqueta;
import es.aglinformatica.rpovedano.bussiness.utils.ConversorTablon;
import es.aglinformatica.rpovedano.bussiness.utils.ConversorUsuario;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.tablon.TablonFormModel;

@Service
public class TablonServiceImpl implements TablonServiceInterface {

	@Autowired
	private TablonDAOInterface tablonDAO;
	@Autowired
	private UsuarioDAOInterface usuarioDAO;
	@Autowired
	private UsuarioTablonDAOInterface usuarioTablonDAO;
	@Autowired
	private EtiquetaTablonDAOInterface etiquetaTablonDAO;

	@Autowired
	private UsuarioServiceInterface usuarioService;
	@Autowired
	private EtiquetaServiceInterface etiquetaService;

	@Autowired
	private ConversorTablon conversorTablon;
	@Autowired
	private ConversorUsuario conversorUsuario;
	@Autowired
	private ConversorEtiqueta conversorEtiqueta;

	@Override
	public List<TablonDto> getListTablonDtoByUsuarioDetails(UsuarioDetails usuarioDetails) {
		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(usuarioDetails.getId());
		List<TablonDto> lista = new ArrayList<TablonDto>();
		if (optionalUser.isPresent()) {
			Usuario usuario = optionalUser.get();
			List<Tablon> listaTablon = tablonDAO.findTablonByUsuario(usuario);
			lista = conversorTablon.listaTablonDto(listaTablon, usuario);

		}
		return lista;
	}

	@Override
	public boolean hasPermission(UsuarioDetails usuarioDetails, Long id) {
		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(usuarioDetails.getId());
		if (optionalUser.isPresent()) {
			Usuario usuario = optionalUser.get();
			Optional<Tablon> optionalTablon = tablonDAO.findTablonByUsuarioAndTablonId(usuario, id);
			return optionalTablon.isPresent();
		} else {
			return false;
		}

	}

	@Override
	@Transactional
	public void crearTablonByUsuario(TablonFormModel tablonFormModel, UsuarioDetails usuarioDetails) {
		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(usuarioDetails.getId());
		if (optionalUser.isPresent()) {
			Usuario usuario = optionalUser.get();
			Tablon tablon = conversorTablon.modeloAEntidad(tablonFormModel, usuario);
			tablon.setNombre(tablonFormModel.getNombre());
			List<Usuario> listaUsuarios = new ArrayList<Usuario>();
			if(!CollectionUtils.isEmpty(tablonFormModel.getLista())) {
				usuarioDAO.findUsuariosByIds(tablonFormModel.getLista());
			}
			List<UsuarioTablon> listaEntidad = new ArrayList<UsuarioTablon>();
			List<EtiquetaTablon> listaEtiqueta = new ArrayList<EtiquetaTablon>();
			listaEntidad = conversorUsuario.listarUsuarioTablon(listaUsuarios, tablon, usuario);
			listaEtiqueta = conversorEtiqueta.listaEtiquetaTablon(tablonFormModel.getListaEtiqueta(), tablon);
			usuarioTablonDAO.saveAll(listaEntidad);
			etiquetaTablonDAO.saveAll(listaEtiqueta);
		}
	}

	@Override
	@Transactional
	public void borrarTablon(Long id) {
		Optional<Tablon> optional = tablonDAO.findById(id);
		if (optional.isPresent()) {
			tablonDAO.deleteById(id);
			tablonDAO.flush();
		}
	}

	@Override
	public TablonFormModel getTablonModelById(Long id, UsuarioDetails usuarioDetails) {
		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(usuarioDetails.getId());
		if (optionalUser.isPresent()) {
			Optional<Tablon> optional = tablonDAO.findById(id);
			if (optional.isPresent()) {
				Tablon tablon = optional.get();
				List<Long> listaUsuario = usuarioService.listarUsuariosIdParaTablon(tablon.getId());
				List<EtiquetaDto> listaEtiqueta = etiquetaService.listarEtiquetasByTablon(tablon.getId());
				TablonFormModel modelo = conversorTablon.entidadAModel(tablon, listaUsuario,listaEtiqueta);
				return modelo;

			}
		}
		return null;
	}

	@Override
	@Transactional
	public void modificarTablon(TablonFormModel tablonFormModel, UsuarioDetails propietario) {
		Optional<Usuario> optionalUser = usuarioDAO.findUsuarioById(propietario.getId());
		if (optionalUser.isPresent()) {
			Usuario usuario = optionalUser.get();
			Optional<Tablon> optional = tablonDAO.findById(tablonFormModel.getId());
			if (optional.isPresent()) {
				Tablon tablon = optional.get();		
				usuarioTablonDAO.deleteAll(tablon.getUsuarioTablon());	
				etiquetaTablonDAO.deleteAll(tablon.getEtiquetaTablon());
				List<Usuario> listaUsuario=usuarioDAO.findUsuariosByIds(tablonFormModel.getLista());
				List<UsuarioTablon> listaUsuarioTablon=conversorUsuario.listarUsuarioTablon(listaUsuario, tablon, usuario);
				List<EtiquetaTablon> listaEtiquetaTablon=conversorEtiqueta.listaEtiquetaTablon(tablonFormModel.getListaEtiqueta(), tablon);
				
				conversorTablon.modificarTablon(tablonFormModel, tablon, usuario);
				tablonDAO.save(tablon);
				usuarioTablonDAO.flush();
				etiquetaTablonDAO.flush();
				if(!CollectionUtils.isEmpty(listaUsuarioTablon)) {
				usuarioTablonDAO.saveAll(listaUsuarioTablon);	
				}
				if(!CollectionUtils.isEmpty(listaUsuarioTablon)) {
				etiquetaTablonDAO.saveAll(listaEtiquetaTablon);
				}
			}
		}

	}

}
