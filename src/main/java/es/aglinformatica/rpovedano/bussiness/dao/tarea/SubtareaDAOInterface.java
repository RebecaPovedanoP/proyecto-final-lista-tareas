package es.aglinformatica.rpovedano.bussiness.dao.tarea;

import org.springframework.data.jpa.repository.JpaRepository;

import es.aglinformatica.rpovedano.bussiness.domain.Subtarea;

public interface SubtareaDAOInterface extends JpaRepository<Subtarea, Long> {

}
