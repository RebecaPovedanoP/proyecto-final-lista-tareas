<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta charset="ISO-8859-1">
<title>Proyecto - Lista de tareas</title>
 <link href="<c:url value="/resources/font-awesome/css/all.css" />" rel="stylesheet">
  <link href="<c:url value="/resources/bootstrap/css/bootstrap.css" />" rel="stylesheet">  
  <link href="<c:url value="/resources/bootstrap/css/bootstrap-select.min.css" />" rel="stylesheet">
  <link href="<c:url value="/resources/bootstrap/css/bootstrap-datepicker.css" />" rel="stylesheet">
 <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet"> 
   <script type="text/javascript" src="<c:url value="/resources/js/jquery-3.6.0.min.js" />"></script>
   <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.bundle.min.js" />"></script>
   <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap-select.js" />"></script>
   <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap-datepicker.js" />"></script>
   <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap-datepicker-es.min.js" />"></script>
   
   <meta name="viewport" content="width=device-width, initial-scale=1">
