# Proyecto Final Lista Tareas

# Base de datos
La base de datos se encuentra embebida en la aplicación. Se hace uso de H2. El script se encuentra en src/main/resources/database/script.sql. Se ha estado trabajando con una base de datos en local, usando como gestor Postgresql. Se pueden encontrar referencias a su uso en el archivo application.properties.

# Framework
El proyecto se ha creado haciendo uso [a link](https://start.spring.io/). El documento pom.xml cuenta con dependencias de maven, para generar el war si hace falta, el propio framework de springboot, spring-security y los repositorios para gestionar las bases de datos, entre otros.

# Webapp
Se está haciendo uso de JSP, con los taglibs correspondientes para dotar de funcionalidad a la aplicación. Para el diseño, se está haciendo uso de bootstrap.

# Notas
Faltan las validaciones de los formularios, sobre todo, el de registro de los usuarios.
Faltan la inserción de todos los datos en tablones y tareas, también falta crear las etiquetas y las subtareas.
Falta rellenar la página de inicio, los colaboradores y la guía.
Cambiar generadores de primary-key.
