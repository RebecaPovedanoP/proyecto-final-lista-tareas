package es.aglinformatica.rpovedano.bussiness.dto;

/**
 * Data Transfer Object de la entidad Tarea
 * @author Rebeca Povedano
 *
 */
public class TareaDto {

	private Long id;
	private String nombre;
	private EtiquetaDto etiqueta;
	private String fechaInicio;
	private String fechaFin;
	private boolean propietario;
	
	public TareaDto() {
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public EtiquetaDto getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(EtiquetaDto etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public boolean isPropietario() {
		return propietario;
	}

	public void setPropietario(boolean propietario) {
		this.propietario = propietario;
	}
}
