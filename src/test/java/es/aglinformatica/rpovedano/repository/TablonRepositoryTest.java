package es.aglinformatica.rpovedano.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import es.aglinformatica.rpovedano.bussiness.dao.tablon.TablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.config.Inicializador;

@SpringBootTest(
		  classes = Inicializador.class)
@TestPropertySource(
		  locations = "classpath:application.properties")
@Transactional
class TablonRepositoryTest {

	@Autowired
	private UsuarioDAOInterface usuarioDAO;
	@Autowired
	private TablonDAOInterface tablonDAO;
	
	
	@Test
	public void tablonByUsuario() {
		Usuario usuario=usuarioDAO.findById(4L).get();
		List<Tablon> lista=tablonDAO.findTablonByUsuario(usuario);
		assertEquals(2,lista.size());
	}

}
