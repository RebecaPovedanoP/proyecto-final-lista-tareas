package es.aglinformatica.rpovedano.bussiness.dao.tablon;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;

@Repository
public interface TablonDAOInterface extends JpaRepository<Tablon, Long> {

	/**
	 * Devuelve la lista de tablones a los que tiene acceso el usuario de la sesión
	 * @param usuario
	 * @return
	 */
	@Query("select distinct(t) from Tablon t left join UsuarioTablon ut on t=ut.tablon left join Usuario u on u=ut.usuario where u=:usuario")
	public List<Tablon> findTablonByUsuario(@Param("usuario") Usuario usuario);
	
	/**
	 * Devuelve el tablón en función de su identificador
	 * @param id
	 * @return
	 */
	@Query("select t from Tablon t join fetch t.usuarioTablon join fetch t.etiquetaTablon where t.id=:id")
	public Optional<Tablon> findTablonById(@Param("id") Long id);
	
	/**
	 * Devuelve el tablón que contiene el identificador pasado como parametro y pertenece al usuario pasado como parametro.
	 * @param usuario
	 * @param id
	 * @return
	 */
	@Query("select t from Tablon t left join UsuarioTablon ut on t=ut.tablon left join Usuario u on u=ut.usuario where u=:usuario and t.id=:id")
	public Optional<Tablon> findTablonByUsuarioAndTablonId(@Param("usuario") Usuario usuario, @Param("id") Long id);
	
	/**
	 * Comprueba si un usuario es propietario del tablón dado.
	 * @param idUsuario
	 * @param idTablon
	 * @return
	 */
	@Query("select ut.propietario from Tablon t join UsuarioTablon ut on ut.tablon=t join Usuario u on u=ut.usuario where u.id=:idUsuario and t.id=:idTablon")
	public Boolean checkTablonUsuarioPropietario(@Param("idUsuario") Long idUsuario, @Param("idTablon") Long idTablon);
}
