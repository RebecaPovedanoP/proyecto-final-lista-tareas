package es.aglinformatica.rpovedano.bussiness.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="tablon")
public class Tablon implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "tablon_seq")
	@SequenceGenerator(name="tablon_seq", initialValue=1, allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@Column(name="nombre", length=100)
	@NotNull
	private String nombre;
	
	@OneToMany(mappedBy = "tablon", cascade = {CascadeType.REFRESH})
    private Set<UsuarioTablon> usuarioTablon;
	
	@OneToMany(mappedBy = "tablon", cascade = {CascadeType.REFRESH})
    private Set<EtiquetaTablon> etiquetaTablon;

	public Tablon() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<UsuarioTablon> getUsuarioTablon() {
		return usuarioTablon;
	}

	public void setUsuarioTablon(Set<UsuarioTablon> usuarioTablon) {
		this.usuarioTablon = usuarioTablon;
	}

	public Set<EtiquetaTablon> getEtiquetaTablon() {
		return etiquetaTablon;
	}

	public void setEtiquetaTablon(Set<EtiquetaTablon> etiquetaTablon) {
		this.etiquetaTablon = etiquetaTablon;
	}
	
	
}
