package es.aglinformatica.rpovedano.bussiness.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="usuario_tablon")
public class UsuarioTablon implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "usuariotablon_seq")
	@SequenceGenerator(name="usuariotablon_seq", initialValue=1, allocationSize=1)
	private Long id;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="id_tablon")
	private Tablon tablon;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Column(name="propietario")
	private boolean propietario;
	
	
	public UsuarioTablon() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public Tablon getTablon() {
		return tablon;
	}

	public void setTablon(Tablon tablon) {
		this.tablon = tablon;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isPropietario() {
		return propietario;
	}

	public void setPropietario(boolean propietario) {
		this.propietario = propietario;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null || this.getClass() != o.getClass()) {
	        return false;
	    }
		UsuarioTablon ut=(UsuarioTablon) o;
		return ut.id.equals(this.id);
	}
	
}
