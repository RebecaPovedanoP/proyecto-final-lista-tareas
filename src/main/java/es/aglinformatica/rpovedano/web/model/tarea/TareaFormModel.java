package es.aglinformatica.rpovedano.web.model.tarea;

import java.util.List;

import es.aglinformatica.rpovedano.bussiness.dto.SubtareaDto;


/**
 * Clase que modela el formulario para la creación y/o modificación de las tareas
 * @author Rebeca Povedano
 *
 */
public class TareaFormModel {

	private Long id;

	private String nombre;

	private String fechaInicio;

	private String fechaFin;
	
	private Long idEtiqueta;
	
	private List<Long> listaUsuarios;
	
	private List<SubtareaDto> listaSubTareas;

	private boolean editar;
	
	public TareaFormModel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Long getIdEtiqueta() {
		return idEtiqueta;
	}

	public void setIdEtiqueta(Long idEtiqueta) {
		this.idEtiqueta = idEtiqueta;
	}

	public List<Long> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Long> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public List<SubtareaDto> getListaSubTareas() {
		return listaSubTareas;
	}

	public void setListaSubTareas(List<SubtareaDto> listaSubTareas) {
		this.listaSubTareas = listaSubTareas;
	}
	
	
}
