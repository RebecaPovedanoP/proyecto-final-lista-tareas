package es.aglinformatica.rpovedano.bussiness.service.tablon;

import java.util.List;

import es.aglinformatica.rpovedano.bussiness.dto.TablonDto;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.tablon.TablonFormModel;

public interface TablonServiceInterface {

	/**
	 * Devuelve el listado del TablonDto según el usuario que esté logueado
	 * @param usuarioDetails
	 * @return
	 */
	public List<TablonDto> getListTablonDtoByUsuarioDetails(UsuarioDetails usuarioDetails);
	
	/**
	 * Compruba si el usuario tiene permiso para acceder al tablón
	 * @param usuarioDetails
	 * @param id
	 * @return
	 */
	public boolean hasPermission(UsuarioDetails usuarioDetails, Long id);	
	
	/**
	 * Obtiene el objeto del formulario para la creación o edición del tablón dado
	 * @param id
	 * @param usuarioDetails
	 * @return
	 */
	public TablonFormModel getTablonModelById(Long id, UsuarioDetails usuarioDetails);

	/**
	 * Inserta en la base de datos la entiedad Tablon, añadiendo sus correspondientes relaciones entre las etiquetas y
	 * los usuarios que pertenecen al mismo
	 * @param tablonFormModel
	 * @param usuarioDetails
	 */
	public void crearTablonByUsuario(TablonFormModel tablonFormModel, UsuarioDetails usuarioDetails);
	
	/**
	 * Modifica los datos de la entidad Tablón según lo que se haya obtenido del formulario y los cambia si se
	 * detecta que los datos difieren de los originales.
	 * @param tablonFormModel
	 * @param propietario
	 */
	public void modificarTablon(TablonFormModel tablonFormModel, UsuarioDetails propietario);
	
	/**
	 * Borra el tablón y todas las entidades que son hijas de este (tarea, subtarea, etiquetas)
	 * @param id
	 */
	public void borrarTablon(Long id);
}
