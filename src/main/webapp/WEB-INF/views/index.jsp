<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../layout/head.jsp" />

</head>
<body>
<jsp:include page="../layout/navbar.jsp" />
<main role="main" class="container">
		<div class="col-12 col-md-12 col-sm-12 col-lg-8 col-xl-8 rounded bg-light p-3 border mx-auto mb-1">
			<h1 class="h2 mb-3 font-weight-normal">
			Bienvenido a la aplicaci�n de gesti�n de tareas.
			</h1>
			
			En ella podr�s crear tus propios tablones, compartirlos con otros usuarios y gestionar tus propias tareas.
			
			
			<h1 class="h3 my-3 font-weight-normal">
			Ejemplos de uso
			</h1>
			
				<h1 class="h4 mb-3 font-weight-normal">
			Tablones en uso
			</h1>
			<img src="<c:url value="/resources/img/tablones.PNG" />" class="img-fluid my-2"/>
			
			
			<div class="my-2"></div>
				<h1 class="h4 mb-3 font-weight-normal">
			Listado de tareas
			</h1>
			<img src="<c:url value="/resources/img/listadotareas.PNG" />" class="img-fluid my-2"/>
			
			<div class="my-2"></div>
			
			
				<h1 class="h4 mb-3 font-weight-normal">
			Edici�n de una tarea, con subtareas incluidas
			</h1>
			<img src="<c:url value="/resources/img/formularioeditar.PNG" />" class="img-fluid my-2"/>
			</div>
</main>

</body>
</html>