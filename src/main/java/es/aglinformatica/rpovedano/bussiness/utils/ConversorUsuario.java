package es.aglinformatica.rpovedano.bussiness.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.bussiness.domain.UsuarioTablon;
import es.aglinformatica.rpovedano.bussiness.dto.UsuarioDto;
/**
 * Componente que hace las conversiones entre las entidades, los dtos y los modelos para la entidad de Usuario
 * @author Rebeca Povedano
 *
 */
@Component
public class ConversorUsuario {
	
	public UsuarioDto entidadADto(Usuario usuario) {
		UsuarioDto dto=new UsuarioDto();
		dto.setId(usuario.getId());
		dto.setUsername(usuario.getUsername());
		return dto;
	}
	
	public List<UsuarioDto> entidadDto(List<Usuario> listaEntidad) {
		List<UsuarioDto> listaDto=new ArrayList<UsuarioDto>();
		for(Usuario usuario : listaEntidad) {
			listaDto.add(entidadADto(usuario));
		}
		return listaDto;
	}
	
	public List<UsuarioTablon> listarUsuarioTablon(List<Usuario> listaUsuario, Tablon tablon, Usuario propietario) {
		List<UsuarioTablon> listaUsuarioTablon=new ArrayList<UsuarioTablon>();
		UsuarioTablon usuarioTablon=new UsuarioTablon();
		usuarioTablon.setTablon(tablon);
		usuarioTablon.setUsuario(propietario);
		usuarioTablon.setPropietario(true);
		listaUsuarioTablon.add(usuarioTablon);
		for(Usuario user : listaUsuario) {
			usuarioTablon=new UsuarioTablon();
			usuarioTablon.setTablon(tablon);
			usuarioTablon.setUsuario(user);		
			listaUsuarioTablon.add(usuarioTablon);
		}
		return listaUsuarioTablon;
	}
	
	
}
