package es.aglinformatica.rpovedano.bussiness.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tarea")
public class Tarea implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "tarea_seq")
	@SequenceGenerator(name="tarea_seq", initialValue=1, allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@NotNull
	@Column(name="nombre", length=200)
	private String nombre;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_Inicio")
	private Date fechaInicio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha_Fin")
	private Date fechaFin;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tablon")
	private Tablon tablon;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_etiqueta")
	private Etiqueta etiqueta;
	
	@OneToMany(mappedBy = "usuario")
    private Set<UsuarioTarea> usuarioTarea;
	

	@OneToMany(mappedBy = "tarea")
    private Set<Subtarea> subTareas;
	
	public Tarea() {
	}
	
	public Tarea(String nombre, Date fechaInicio, Date fechaFin, Tablon tablon, Etiqueta etiqueta) {
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.tablon = tablon;
		this.etiqueta = etiqueta;
	}

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Tablon getTablon() {
		return tablon;
	}

	public void setTablon(Tablon tablon) {
		this.tablon = tablon;
	}

	public Etiqueta getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(Etiqueta etiqueta) {
		this.etiqueta = etiqueta;
	}

	public Set<UsuarioTarea> getUsuarioTarea() {
		return usuarioTarea;
	}

	public void setUsuarioTarea(Set<UsuarioTarea> usuarioTarea) {
		this.usuarioTarea = usuarioTarea;
	}

	public Set<Subtarea> getSubTareas() {
		return subTareas;
	}

	public void setSubTareas(Set<Subtarea> subTareas) {
		this.subTareas = subTareas;
	}
	
}
