package es.aglinformatica.rpovedano.bussiness.service.tarea;

import java.util.List;

import es.aglinformatica.rpovedano.bussiness.dto.TareaDto;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.tarea.TareaFormModel;

public interface TareaServiceInterface {

	/**
	 * Devuelve el listado de las tareas según el tablón y el usuario conectado
	 * @param idTablon
	 * @param usuarioDetails
	 * @return
	 */
	public List<TareaDto> getListTareaDtoByTablonId(Long idTablon, UsuarioDetails usuarioDetails);
	
	/**
	 * Devuelve el listado de las tareas pendientes según el usuario que esté coneectado
	 * @param usuarioDetails
	 * @return
	 */
	public List<TareaDto> getListTareaDtoPendiente(UsuarioDetails usuarioDetails);

	/**
	 * Compruba si el usuario tiene permiso para acceder a la tarea
	 * @param usuarioDetails
	 * @param id
	 * @return
	 */
	public boolean hasPermission(UsuarioDetails usuarioDetails, Long id);	
	/**
	 * Obtiene el objeto del formulario para la creación o edición de la tarea dada
	 * @param idTarea
	 * @param propietario
	 * @return
	 */
	public TareaFormModel getTareaModelById(Long idTarea, UsuarioDetails propietario);
	/**
	 * Inserta en la base de datos la entidad Tarea, añadiendo sus correspondientes relaciones con la etiqueta y los usuarios
	 * asignados a la misma. También, si el usuario así lo desea, se procederá a la creación de las subtareas
	 * @param idTablon
	 * @param usuarioDetails
	 * @param tareaFormModel
	 */
	public void createTarea(Long idTablon, UsuarioDetails usuarioDetails, TareaFormModel tareaFormModel);
	
	/**
	 * Modifica los datos de la entidad Tarea según lo que se haya obtenido del formulario y los cambia si se
	 * detecta que los datos difieren de los originales.
	 * @param usuarioDetails
	 * @param tareaFormModel
	 */
	public void modificarTarea(UsuarioDetails usuarioDetails, TareaFormModel tareaFormModel);
	
	/**
	 * Borra el la tarea y todas las entidades que son hijas de este (tarea, subtarea, etiquetas)
	 * @param id
	 */
	public void borrarTarea(Long id);
	
}
