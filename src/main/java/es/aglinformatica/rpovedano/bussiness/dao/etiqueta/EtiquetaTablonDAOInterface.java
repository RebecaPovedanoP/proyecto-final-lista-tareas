package es.aglinformatica.rpovedano.bussiness.dao.etiqueta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.aglinformatica.rpovedano.bussiness.domain.EtiquetaTablon;

@Repository
public interface EtiquetaTablonDAOInterface extends JpaRepository<EtiquetaTablon, Long> {

}
