package es.aglinformatica.rpovedano.web.controller.main;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;
import es.aglinformatica.rpovedano.bussiness.dto.TareaDto;
import es.aglinformatica.rpovedano.bussiness.dto.UsuarioDto;
import es.aglinformatica.rpovedano.bussiness.service.tarea.TareaServiceInterface;
import es.aglinformatica.rpovedano.bussiness.service.usuario.UsuarioServiceInterface;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.tablon.TablonFormModel;

/**
 * Controlador que gestiona el acceso al indice
 * @author Rebeca Povedano
 *
 */
@Controller
@RequestMapping("/")
public class MainController {
	@Autowired
	private TareaServiceInterface tareaService;

	@Autowired
	private UsuarioServiceInterface usuarioService;

	private final String TAREA_JSP="/tarea/lista-tarea";
	
	/**
	 * Petición get que muestra el indice
	 * @param model
	 * @return
	 */
	@GetMapping("")
	public String getIndex(Model model) {
		
		return "index";
	}
	
	/**
	 * Petición get que muestra el indice
	 * @param model
	 * @return
	 */
	@GetMapping("/guia")
	public String getGuia(Model model) {		
		return "guia";
	}
	
	
	/**
	 * Petición get que muestra la lista de tareas pendientes
	 * @param model
	 * @param authentication
	 * @return
	 */
	@GetMapping("/tarea-pendiente")
	public String getTareaPendiente(Model model, Authentication authentication) {		
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
			List<TareaDto> lista=tareaService.getListTareaDtoPendiente(usuarioDetails);
		    model.addAttribute("listaTarea", lista);
		    model.addAttribute("pendiente", true);
			} else {
				return "error";
			}
		
		return TAREA_JSP;
	}
	
	@GetMapping("/colaborador")
	public String getColaboradores(Model model, Authentication authentication) {		
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UsuarioDetails usuarioDetails= (UsuarioDetails) authentication.getPrincipal();
			List<UsuarioDto> lista=usuarioService.listarUsuariosColaboradores(usuarioDetails);
			model.addAttribute("lista", lista);
			model.addAttribute("vacio",lista.isEmpty());
			} else {
				return "error";
			}
		
		return "colaborador";
	}

}
