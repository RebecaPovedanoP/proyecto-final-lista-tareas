package es.aglinformatica.rpovedano.bussiness.dao.tarea;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Tarea;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;

@Repository
public interface TareaDAOInterface extends JpaRepository<Tarea, Long> {

	/**
	 * Devuelve todas las tareas que pertenecen al tablón pasado como parametro
	 * @param tablon
	 * @return
	 */
	@Query("select tr from Tarea tr join fetch tr.tablon t join fetch tr.etiqueta where t=:tablon order by tr.etiqueta")
	public List<Tarea> findAllTareasByTablon(@Param("tablon") Tablon tablon);

	/**
	 * Devuelve todas aquellas tareas que están pendientes de realizarse para un usuario en concreto, independientemente de su
	 * tablon
	 * @param idUsuario
	 * @return
	 */
	@Query("select distinct(t) from Tarea t join UsuarioTarea ut on t=ut.tarea where ut.usuario.id=:idUsuario and (t.fechaFin is null or cast(:fechaActual as timestamp) <t.fechaFin)")
	public List<Tarea> findTareasPendientes(@Param("idUsuario") Long idUsuario, @Param("fechaActual") Date fechaActual);
	
	/**
	 * Devuelve la tarea que contiene el identificador pasado como parametro y pertenece al usuario pasado como parametro.
	 * @param usuario
	 * @param id
	 * @return
	 */
	@Query("select t from Tarea t left join UsuarioTarea ut on t=ut.tarea left join Usuario u on u=ut.usuario where u=:usuario and t.id=:id")
	public Optional<Tarea> findTareaByUsuarioAndTareaId(@Param("usuario") Usuario usuario, @Param("id") Long id);
	
	/**
	 * Comprueba si el usuario es propietario o no de la tarea pasada como parametro.
	 * @param idUsuario
	 * @param idTarea
	 * @return
	 */
	@Query("select ut.propietario from Tarea t join UsuarioTarea ut on ut.tarea=t where ut.usuario.id=:idUsuario and t.id=:idTarea")
	public Boolean checkTareaUsuarioPropietario(@Param("idUsuario") Long idUsuario, @Param("idTarea") Long idTarea);
	
	
}
