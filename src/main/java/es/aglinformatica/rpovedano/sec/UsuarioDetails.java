package es.aglinformatica.rpovedano.sec;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import es.aglinformatica.rpovedano.bussiness.domain.Usuario;

/**
 * Clase que implementa UserDetails, para poder obtener la información del usuario conectador. Se han dejado la mayor parte
 * de las variables en true para evitar problemas a la hora de gestionar la conexión.
 * @author Rebeca
 *
 */
public class UsuarioDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Usuario usuario;
	
	private Long idTablon;
	
	public UsuarioDetails(Usuario usuario) {
		this.usuario=usuario;
		
	}

	public Long getId() {
		return usuario.getId();
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return usuario.getPass();
	}

	@Override
	public String getUsername() {
		return usuario.getUsername();
	}
	
	

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public Long getIdTablon() {
		return idTablon;
	}

	/**
	 * Permite guardar el identificador del tablón y usarlo en otras páginas.
	 * @param idTablon
	 */
	public void setIdTablon(Long idTablon) {
		this.idTablon = idTablon;
	}

}
