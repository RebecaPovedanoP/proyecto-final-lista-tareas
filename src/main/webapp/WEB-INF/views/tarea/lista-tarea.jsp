<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<jsp:include page="../../layout/head.jsp" />
<script>
	$('.selectpicker').selectpicker();
	 function postBorrar(id){
		 if(confirm("�Desea borrar esta tarea?") ==true) {
			 var url=window.location.pathname+"/borrar/"+id;
			 $.post(url); 
		 }    
	  }
</script>
</head>
<body>
	<jsp:include page="../../layout/navbar.jsp" />
	<main role="main" class="container">
		<h1 class="h3 mb-3 font-weight-normal">
			<spring:message code="tarea.titulo" />
		</h1>
		<c:if test="${!pendiente}">
		<div class="clearfix mb-3">
			<div class="float-right">
				<c:url var="crear" value="/tarea/crear" />
				<a class="btn-primary btn btn-lg" href="${crear}"> <spring:message
						code="tarea.boton.crear" />
				</a>
			</div>
		</div></c:if>

		<div class="row mx-auto">
		<div class="col-12 bg-dark text-light row h5 mx-auto p-3 rounded ${empty listaTarea ? 'd-none' : ''}">
		<div class="col-3 px-4">Nombre de la tarea</div><div class="col-3">Etiqueta</div><div class="col-2">Fecha de inicio</div><div class="col-3">Fecha de finalizaci�n</div>
		</div>
			<c:forEach var="tarea" varStatus="status" items="${listaTarea}">
				<div class="col-12 bg-light rounded p-1 border text-dark mb-2">
					<div class="row">
						<div class="col-3 px-4">
							<h1 class="h5"><c:out value="${tarea.nombre}" /></h1>
						</div>
						<div class="col-3">
								<select class="selectpicker"
									data-style="bg-white text-dark form-control border btn-block" disabled>

									
												<option value="${tarea.etiqueta.id}"
													data-content="<span class='btn' style='background: ${tarea.etiqueta.color}'></span>  ${tarea.etiqueta.nombre}"></option>
										
								</select>

						</div>
						<div class="col-2 text-center">
						<c:out value="${tarea.fechaInicio}"/>
						
						</div>
						
						<div class="col-2">
						<c:out value="${tarea.fechaFin}"/>
						
						</div>
						<div class="col-2 text-right pr-4">
						<c:if test="${!pendiente}">
						<a href="<c:url value="/tarea/editar/${tarea.id}"/>" class="rounded-circle bg-dark text-light p-2 d-inline-block" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="tarea.boton.editar"/>" ><i class="fas fa-edit"></i></a>
						
						<c:if test="${tarea.propietario}">
		<a href="javascript:postBorrar(${tarea.id})" class="borrarTarea rounded-circle bg-dark text-light p-2 d-inline-block" data-toggle="tooltip" data-placement="bottom" title="<spring:message code="tarea.boton.borrar"/>"><i class="fas fa-trash-alt"></i></a>
						</c:if>
						</c:if>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</main>
</body>
</html>