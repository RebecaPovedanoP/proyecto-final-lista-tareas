CREATE TABLE usuario (
    id BIGINT NOT NULL,
    username VARCHAR(100) NOT NULL UNIQUE,
    correo VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(150) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE tablon (
    id BIGINT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE usuario_tablon (
    id BIGINT NOT NULL,
    id_tablon BIGINT NOT NULL,
    id_usuario BIGINT NOT NULL,
    propietario BOOLEAN DEFAULT false,
    PRIMARY KEY(id),
    UNIQUE(id_tablon,id_usuario),
    FOREIGN KEY (id_tablon) REFERENCES tablon(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE etiqueta (
    id BIGINT NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    color VARCHAR(20) DEFAULT 'GREY',
    PRIMARY KEY(id)
);
CREATE TABLE etiqueta_tablon (
    id BIGINT NOT NULL,
    id_etiqueta BIGINT NOT NULL,
    id_tablon BIGINT NOT NULL,
    PRIMARY KEY(id),
    UNIQUE(id_etiqueta,id_tablon),
    FOREIGN KEY (id_tablon) REFERENCES tablon(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_etiqueta) REFERENCES etiqueta(id) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE tarea (
    id BIGINT NOT NULL,
    nombre VARCHAR(200) NOT NULL,
    fecha_Inicio DATE,
    fecha_Fin DATE,
    id_tablon BIGINT NOT NULL,
    id_etiqueta BIGINT,
    PRIMARY KEY(id),
    FOREIGN KEY (id_tablon) REFERENCES tablon(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_etiqueta) REFERENCES etiqueta(id) ON UPDATE CASCADE ON DELETE CASCADE        
);


CREATE TABLE usuario_tarea(
    id BIGINT NOT NULL,
    id_usuario BIGINT NOT NULL,
    id_tarea BIGINT NOT NULL,
    propietario BOOLEAN DEFAULT false,
    PRIMARY KEY(id),
    UNIQUE(id_tarea,id_usuario),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_tarea) REFERENCES tarea(id) ON UPDATE CASCADE ON DELETE CASCADE
);



CREATE TABLE subtarea (
    id BIGINT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    estado BOOLEAN DEFAULT FALSE,
    id_tarea BIGINT NOT NULL,
    id_usuario BIGINT,
    PRIMARY KEY(id),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_tarea) REFERENCES tarea(id) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE SEQUENCE usuario_seq START WITH 4 INCREMENT BY 1;
CREATE SEQUENCE tablon_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE usuariotablon_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE etiqueta_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE etiquetatablon_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE tarea_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE usuariotarea_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE subtarea_seq START WITH 1 INCREMENT BY 1;

INSERT INTO usuario values(1,'usuario1','usuario1@correo.es','$2y$12$El99ahQjPlN9wOBXKCmg0.q95MJAdQDAKHSOGg.AM.nhxKcxaSqXu');
INSERT INTO usuario values(2,'usuario2','usuario2@correo.es','$2y$12$El99ahQjPlN9wOBXKCmg0.q95MJAdQDAKHSOGg.AM.nhxKcxaSqXu');
INSERT INTO usuario values(3,'usuario3','usuario3@correo.es','$2y$12$El99ahQjPlN9wOBXKCmg0.q95MJAdQDAKHSOGg.AM.nhxKcxaSqXu');