/**
 * 
 */

$( document ).ready(function() {
    $('#etiquetaTablon').submit(function( event ) {
	  event.preventDefault();
	  var action=$(this).attr("action");
	  var formData=$(this).serialize();
	  $.post(action, formData, function(data) {
		  if(data.nombre!=null && data.nombre!=="") {
		  $('#validacionMensaje').remove();
		  $('#etiquetaTabla').removeClass("d-none");		  
		  $('h1.h5').removeClass("d-none");
		  var n=$('.listaEtiqueta.nombre').length;
		  var nombre='<input name="listaEtiqueta['+n+'].nombre" class="listaEtiqueta nombre" type="hidden" value="'+data.nombre+'">';
		  var color='<input name="listaEtiqueta['+n+'].color" class="listaEtiqueta color" type="hidden" value="'+data.color+'">';
		  $('#etiquetaTabla').after(nombre);
		  $('#etiquetaTabla').after(color);
		  $('#etiquetaTabla tbody').append('<tr><td>'+data.nombre+'</td><td><div class="btn col-8 p-3" style="background:'+data.color+'"></div></td></tr>')
		  $('#etiquetaTablon input#nombre').val('');
		  $('#etiquetaTablon input#color').val('');
		  
	  } else {
		  if($('#validacionMensaje').length==0) {
		  	  $('#etiquetaTablon').after("<div id='validacionMensaje' class='mb-3 alert mt-2 alert-warning d-block p-2'>El nombre de la etiqueta es obligatorio</div>");
		  } else {
		  $('#validacionMensaje').remove();
		  }
	  }
		  });
	});

});