package es.aglinformatica.rpovedano.bussiness.dao.usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;

@Repository
public interface UsuarioDAOInterface extends JpaRepository<Usuario,Long> {
	
	/**
	 * Devuelve la lista de usuarios
	 * @return
	 */
	@Query("select u from Usuario u")
	public List<Usuario> findAllUsuario();

	/**
	 * Devuelve el optional usuario por su identificador
	 * @param id
	 * @return
	 */
	@Query("select u from Usuario u where u.id=:id")
	public Optional<Usuario> findUsuarioById(@Param("id") Long id);
	
	/**
	 * Devuelve el optional del usuario según su username
	 * @param username
	 * @return
	 */
	@Query("select u from Usuario u where u.username=:username")
	public Optional<Usuario> findUsuarioByUsername(@Param("username") String username);
	
	/**
	 * Devuelve la lista de usuarios cuyo identificador pertenezca a la lista pasada como parametro
	 * @param lista
	 * @return
	 */
	@Query("select u from Usuario u where u.id in :lista")
	public List<Usuario> findUsuariosByIds(@Param("lista") List<Long> lista);
	
	/**
	 * Devuelve una lista de usuarios que no contiene al usuario cuyo identifiador se ha pasado por parametro
	 * @param id
	 * @return
	 */
	@Query("select u from Usuario u where u.id!=:id")
	public List<Usuario> findAllUsuarioNoPropio(@Param("id") Long id);
	
	/**
	 * Devuelve el optional del usuario según su correo
	 * @param correo
	 * @return
	 */
	@Query("select u from Usuario u where u.correo=:correo")
	public Optional<Usuario> findUsuarioByCorreo(@Param("correo") String correo);
	
	/**
	 * Devuelve la lista de usuarios que pertenecen al tablón, excepto el que se ha pasado como parámetro
	 * @param idTablon
	 * @param idUsuario
	 * @return
	 */
	@Query("select u from Usuario u join UsuarioTablon ut on u=ut.usuario where u.id!=:idUsuario and ut.tablon.id=:idTablon")
	public List<Usuario> findUsuariosNoPropioByIdTablon(@Param("idTablon") Long idTablon, @Param("idUsuario") Long idUsuario);
	
	/**
	 * Devuelve la lista de usuarios que pertenecen al tablon
	 * @param idTablon
	 * @return
	 */
	@Query("select u from Usuario u join UsuarioTablon ut on u=ut.usuario where ut.tablon.id=:idTablon")
	public List<Usuario> findUsuariosByIdTablon(@Param("idTablon") Long idTablon);
	
	/**
	 * Devuelve la lista de usuarios que pertenecen a la tarea
	 * @param idTarea
	 * @return
	 */
	@Query("select u from Usuario u join UsuarioTarea ut on u=ut.usuario join Tarea t on t=ut.tarea where t.id=:idTarea")
	public List<Usuario> findUsuariosByIdTarea(@Param("idTarea") Long idTarea);
	/**
	 * Devuelve el listado de usuarios que comparten alguno de los tablones de la lista con el usuario dado
	 * @param idUsuario
	 * @param lista
	 * @return
	 */
	@Query("select distinct(u) from Usuario u join UsuarioTablon ut on u=ut.usuario where u.id!=:idUsuario and ut.tablon in :lista")
	public List<Usuario> findColaboradores(@Param("idUsuario") Long idUsuario, @Param("lista") List<Tablon> lista);
}
