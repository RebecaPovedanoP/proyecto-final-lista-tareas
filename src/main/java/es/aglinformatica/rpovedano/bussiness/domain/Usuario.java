package es.aglinformatica.rpovedano.bussiness.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="usuario")
public class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "usuario_seq")
	@SequenceGenerator(name="usuario_seq", initialValue=1, allocationSize=1)
	@Column(name="id")
	private Long id;
	
	@Column(name="username")
	@NotNull
	private String username;
	
	@Column(name="correo")
	@NotNull
	private String correo;
	
	@Column(name="password")
	@NotNull
	private String pass;
	
	@OneToMany(mappedBy = "usuario")
    private Set<UsuarioTablon> usuarioTablon;
	

	@OneToMany(mappedBy = "usuario")
    private Set<UsuarioTarea> usuarioTarea;
	
	public Usuario() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Set<UsuarioTablon> getUsuarioTablon() {
		return usuarioTablon;
	}

	public void setUsuarioTablon(Set<UsuarioTablon> usuarioTablon) {
		this.usuarioTablon = usuarioTablon;
	}
	
	public Set<UsuarioTarea> getUsuarioTarea() {
		return usuarioTarea;
	}
	
	public void setUsuarioTarea(Set<UsuarioTarea> usuarioTarea) {
		this.usuarioTarea = usuarioTarea;
	}
}
