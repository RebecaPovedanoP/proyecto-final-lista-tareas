package es.aglinformatica.rpovedano.bussiness.dto;

/**
 * Data Transfer Object de la entidad Usuario
 * @author Rebeca Povedano
 *
 */
public class UsuarioDto {

	private Long id;
	private String username;
	
	public UsuarioDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
