package es.aglinformatica.rpovedano.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import es.aglinformatica.rpovedano.bussiness.dao.etiqueta.EtiquetaDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tablon.TablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.tarea.TareaDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Etiqueta;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Tarea;
import es.aglinformatica.rpovedano.config.Inicializador;

@SpringBootTest(
		  classes = Inicializador.class)
@TestPropertySource(
		  locations = "classpath:application.properties")
@Transactional
class TareaRepositoryTest {

	@Autowired
	private TareaDAOInterface tareaDAO;
	@Autowired
	private TablonDAOInterface tablonDAO;

	@Autowired
	private EtiquetaDAOInterface etiquetaDAO;
	
	@Test
	public void tablonByUsuario() {
		Tablon tablon=tablonDAO.getOne(1L);
		List<Tarea> lista=tareaDAO.findAllTareasByTablon(tablon);
		assertEquals(1,lista.size());
	}
	
	
	@Test
	public void etiquetaByTablon() {
		Tablon tablon=tablonDAO.getOne(16L);
		List<Etiqueta> lista=etiquetaDAO.listarEtiquetaByTablon(tablon);
		assertEquals(3,lista.size());
	}
}
