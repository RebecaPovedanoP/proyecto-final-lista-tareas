package es.aglinformatica.rpovedano.web.validador;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.aglinformatica.rpovedano.web.model.tablon.TablonFormModel;


/**
 * Validador para la clase TareaFormModel. Realiza las validaciones de las fechas, que estén en un formato correcto,
 * que la fecha de inicio no sea mayor que la fecha de finalización, que se seleccione una etiqueta de forma obligatoria
 * y que el nombre  de la etiqueta no pueda estar vacio.
 * @author Rebeca Povedano
 *
 */
@Component
public class TablonValidador implements Validator {
	boolean finalizado = false;

	@Override
	public boolean supports(Class<?> clazz) {

		return TablonFormModel.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		TablonFormModel modelo = (TablonFormModel) target;

		validateNombre(modelo, errors);
		validateEtiquetas(modelo, errors);
	}

	private void validateNombre(TablonFormModel modelo, Errors errors) {
		if (StringUtils.isAllBlank(modelo.getNombre())) {
			errors.rejectValue("nombre", "input.text.error.vacio");
		}
	}

	private void validateEtiquetas(TablonFormModel modelo, Errors errors) {
		if (CollectionUtils.isEmpty(modelo.getListaEtiqueta())) {
			errors.rejectValue("listaEtiqueta", "tablon.input.etiquetas.error");
		} 
	}

}
