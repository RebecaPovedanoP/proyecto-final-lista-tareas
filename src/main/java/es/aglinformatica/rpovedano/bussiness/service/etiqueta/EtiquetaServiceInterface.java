package es.aglinformatica.rpovedano.bussiness.service.etiqueta;

import java.util.List;

import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;

public interface EtiquetaServiceInterface {

	/**
	 * Devuelve el listado de EtiquetaDto según el id del tablon
	 * @param idTablon
	 * @return
	 */
	public List<EtiquetaDto> listarEtiquetasByTablon(Long idTablon);
}
