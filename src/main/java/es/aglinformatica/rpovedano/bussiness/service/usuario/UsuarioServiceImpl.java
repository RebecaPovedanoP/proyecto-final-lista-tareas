package es.aglinformatica.rpovedano.bussiness.service.usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import es.aglinformatica.rpovedano.bussiness.dao.tablon.TablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.bussiness.dto.UsuarioDto;
import es.aglinformatica.rpovedano.bussiness.utils.ConversorUsuario;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.usuario.UsuarioLoginFormModel;
import es.aglinformatica.rpovedano.web.model.usuario.UsuarioRegistroFormModel;

@Service
public class UsuarioServiceImpl implements UsuarioServiceInterface {

	@Autowired
	private UsuarioDAOInterface usuarioDAO;
	@Autowired
	private TablonDAOInterface tablonDAO;
	
	@Autowired
	private ConversorUsuario conversorUsuario;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	
	@Override
	public void crearUser(UsuarioRegistroFormModel usuarioModel) {
		Usuario usuario=new Usuario();
		usuario.setUsername(usuarioModel.getUsername());
		usuario.setCorreo(usuarioModel.getCorreo());
		usuario.setPass(passwordEncoder.encode(usuarioModel.getPassword()));
		usuarioDAO.save(usuario);
	}

	@Override
	public void modificarUsuario(UsuarioRegistroFormModel usuarioModel) {
		Optional<Usuario> optional=usuarioDAO.findById(usuarioModel.getId());
		
		if(optional.isPresent()) {
			Usuario usuario=optional.get();
			usuario.setUsername(usuarioModel.getUsername());
			usuario.setCorreo(usuarioModel.getCorreo());
			usuario.setPass(passwordEncoder.encode(usuarioModel.getPassword()));
			usuarioDAO.save(usuario);
		} else {
			System.out.println("No existe el usuario.");
		}
	}

	@Override
	public boolean comprobarCrendenciales(UsuarioLoginFormModel usuarioModel) {
		Optional<Usuario> optional=usuarioDAO.findUsuarioByUsername(usuarioModel.getUsername());
		if(optional.isPresent()) {
			Usuario usuario=optional.get();
			if(usuario.getPass().equals(passwordEncoder.encode(usuarioModel.getPassword()))) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}

	@Override
	public List<UsuarioDto> listarUsuarioDtoParaTablon(UsuarioDetails usuarioDetails) {
		Long id=usuarioDetails.getId();
		List<Usuario> listaUsuario=usuarioDAO.findAllUsuarioNoPropio(id);
		List<UsuarioDto> lista=new ArrayList<UsuarioDto>();
		listaUsuario.forEach(usuario ->{
			UsuarioDto dto=new UsuarioDto();
			dto.setId(usuario.getId());
			dto.setUsername(usuario.getUsername());
			lista.add(dto);
		});
		return lista;
	}

	@Override
	public List<UsuarioDto> listarUsuarioDtoParaTarea(UsuarioDetails usuarioDetails, Long idTablon) {
		Long idUsuario=usuarioDetails.getId();
		List<Usuario> listaUsuario=usuarioDAO.findUsuariosNoPropioByIdTablon(idTablon, idUsuario);
		List<UsuarioDto> lista=conversorUsuario.entidadDto(listaUsuario);
		return lista;
	}

	@Override
	public List<Long> listarUsuariosIdParaTablon(Long idTablon) {
		List<Usuario> listaUsuario=usuarioDAO.findUsuariosByIdTablon(idTablon);
		List<Long> lista=new ArrayList<Long>();
		listaUsuario.forEach(usuario ->lista.add(usuario.getId()));
		return lista;
	}


	@Override
	public List<Long> listarUsuariosIdParaTarea(Long idTarea) {
		List<Usuario> listaUsuario=usuarioDAO.findUsuariosByIdTarea(idTarea);
		List<Long> lista=new ArrayList<Long>();
		listaUsuario.forEach(usuario ->lista.add(usuario.getId()));
		return lista;
	}
	
	@Override
	public List<UsuarioDto> listarUsuariosColaboradores(UsuarioDetails usuarioDetails) {
		Optional<Usuario> optionalUsuario=usuarioDAO.findById(usuarioDetails.getId());
		
		if(optionalUsuario.isPresent()) {
			List<Tablon> listaTablon=tablonDAO.findTablonByUsuario(optionalUsuario.get());
			List<Usuario> listaUsuario=usuarioDAO.findColaboradores(usuarioDetails.getId(), listaTablon);
			return conversorUsuario.entidadDto(listaUsuario);
		} else {
			return null;
		}
		
	}


}
