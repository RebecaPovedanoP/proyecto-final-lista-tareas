package es.aglinformatica.rpovedano.bussiness.service.usuario;

import java.util.List;

import es.aglinformatica.rpovedano.bussiness.dto.UsuarioDto;
import es.aglinformatica.rpovedano.sec.UsuarioDetails;
import es.aglinformatica.rpovedano.web.model.usuario.UsuarioLoginFormModel;
import es.aglinformatica.rpovedano.web.model.usuario.UsuarioRegistroFormModel;

public interface UsuarioServiceInterface {

	/**
	 * Crea un usuario obtieniendo los datos del formulario de registro
	 * @param usuarioModel
	 */
	public void crearUser(UsuarioRegistroFormModel usuarioModel);
	
	public void modificarUsuario(UsuarioRegistroFormModel usuarioModel);
		
	/**
	 * Consulta si las credenciales proporcionadas por el usuario a la hora de conectarse son correctas o no
	 * @param usuarioModel
	 * @return
	 */
	public boolean comprobarCrendenciales (UsuarioLoginFormModel usuarioModel);

	/**
	 * Devuelve la lista de las ids de los usuarios que pertenecen al tablón y que se usan en el modelo del formulario para
	 * la posterior edición del mismo.
	 * @param idTablon
	 * @return
	 */
	public List<Long> listarUsuariosIdParaTablon(Long idTablon);
	
	/**
	 * Devuelve la lista de las ids de los usuarios que pertenecen a la tarea y que se usan en el modelo del formulario para
	 * la posterior edición de la mismo.
	 * @param idTablon
	 * @return
	 */
	public List<Long> listarUsuariosIdParaTarea(Long idTarea);
	
	/**
	 * Devuelve la lista de los usuarios que se muestran en la creacción del tablón, con todos sus datos relevantes.
	 * @param usuarioDetails
	 * @return
	 */
	public List<UsuarioDto> listarUsuarioDtoParaTablon (UsuarioDetails usuarioDetails);	

	/**
	 * Devuelve la lista de los usuarios que pueden acceder al tablón al que pertenece la tarea.
	 * @param usuarioDetails
	 * @param idTablon
	 * @return
	 */
	public List<UsuarioDto> listarUsuarioDtoParaTarea (UsuarioDetails usuarioDetails, Long idTablon);
	
	/**
	 * Devuelve la lista de los usuarios que compartes tablones con el logueado
	 * @param usuarioDetails
	 * @return
	 */
	public List<UsuarioDto> listarUsuariosColaboradores(UsuarioDetails usuarioDetails);
	
}
