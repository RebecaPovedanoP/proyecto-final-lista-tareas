package es.aglinformatica.rpovedano.web.model.usuario;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Clase que modela el formulario para el login del usuario
 * @author Rebeca Povedano
 *
 */
public class UsuarioLoginFormModel {
	private Long id;
	
	@NotNull
	@NotEmpty (message = "{input.text.error.vacio}")
	private String username;

	
	@NotNull
	@NotEmpty (message = "{input.text.error.vacio}")
	private String password;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
