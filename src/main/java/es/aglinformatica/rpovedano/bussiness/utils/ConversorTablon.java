package es.aglinformatica.rpovedano.bussiness.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.aglinformatica.rpovedano.bussiness.dao.tablon.TablonDAOInterface;
import es.aglinformatica.rpovedano.bussiness.dao.usuario.UsuarioDAOInterface;
import es.aglinformatica.rpovedano.bussiness.domain.EtiquetaTablon;
import es.aglinformatica.rpovedano.bussiness.domain.Tablon;
import es.aglinformatica.rpovedano.bussiness.domain.Usuario;
import es.aglinformatica.rpovedano.bussiness.domain.UsuarioTablon;
import es.aglinformatica.rpovedano.bussiness.dto.EtiquetaDto;
import es.aglinformatica.rpovedano.bussiness.dto.TablonDto;
import es.aglinformatica.rpovedano.web.model.tablon.TablonFormModel;
/**
 * Componente que hace las conversiones entre las entidades, los dtos y los modelos para la entidad de Tablon
 * @author Rebeca Povedano
 *
 */
@Component
public class ConversorTablon {

	@Autowired
	private UsuarioDAOInterface usuarioDAO;
	@Autowired
	private TablonDAOInterface tablonDAO;
	
	@Autowired
	private ConversorEtiqueta conversorEtiqueta;
	@Autowired
	private ConversorUsuario conversorUsuario;
	
	
	public Tablon modeloAEntidad(TablonFormModel modelo, Usuario propietario) {
		Tablon tablon=new Tablon();
		tablon.setId(modelo.getId());
		tablon.setNombre(modelo.getNombre());
		List<Usuario> listaUsuario=usuarioDAO.findUsuariosByIds(modelo.getLista());
		List<UsuarioTablon> listaUsuarioTablon=conversorUsuario.listarUsuarioTablon(listaUsuario, tablon, propietario);
		tablon.setUsuarioTablon(new HashSet<UsuarioTablon>(listaUsuarioTablon));
		List<EtiquetaTablon> listaEtiquetaTablon=conversorEtiqueta.listaEtiquetaTablon(modelo.getListaEtiqueta(), tablon);
		tablon.setEtiquetaTablon(new HashSet<EtiquetaTablon>(listaEtiquetaTablon));
		return tablon;
	}
	
	public TablonFormModel entidadAModel(Tablon tablon, List<Long> listaUsuario, List<EtiquetaDto> listaEtiqueta) { 
		TablonFormModel modelo=new TablonFormModel();
		modelo.setId(tablon.getId());
		modelo.setNombre(tablon.getNombre());
		modelo.setLista(listaUsuario);
		modelo.setListaEtiqueta(listaEtiqueta);
		return modelo;
	}
	
	public void modificarTablon(TablonFormModel modelo, Tablon tablon, Usuario propietario) {
		tablon.setId(modelo.getId());
		tablon.setNombre(modelo.getNombre());
	}
	
	public TablonDto entidadADto(Tablon tablon, Usuario usuario) {
		TablonDto dto=new TablonDto();
		dto.setId(tablon.getId());
		dto.setNombre(tablon.getNombre());
		Boolean propietario=tablonDAO.checkTablonUsuarioPropietario(usuario.getId(),tablon.getId());
		dto.setPropietario(propietario !=null ? propietario.booleanValue() : false);
		return dto;
	}
	
	public List<TablonDto> listaTablonDto (List<Tablon> listaTablon, Usuario usuario) {
		List<TablonDto> listaDto=new ArrayList<TablonDto>();
		for(Tablon tablon : listaTablon) {
			listaDto.add(entidadADto(tablon, usuario));
		}
		return listaDto;
	}
}
